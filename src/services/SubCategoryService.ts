import callApi from "@/lib/axios";

export const SubCategoryService = {
    getAll: async () : Promise<SubCategory[]> => {
        try {
            const res = await callApi.get("categories/sub_categories/?is_sub=true");
            if(res.status === 200 && res.data !== null){
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },
};