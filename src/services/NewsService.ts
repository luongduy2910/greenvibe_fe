import callApi from "@/lib/axios";
import { News } from "@/types/news";

export const NewsService = {
    getBanner: async () : Promise<News[]> => {
        try {
            const res = await callApi.get("news/get_news/");
            if(res.status === 200 && res.data != null){
                return res.data;
            }else{
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },
};