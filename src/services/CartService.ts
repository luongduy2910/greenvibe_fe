import callApi from "@/lib/axios"
import { AddToCart, CartDetails, CartItems } from "@/types/cart";
import React from "react";

export const CartService = {

    addToCart: async (req: AddToCart) : Promise<CartDetails[]> => {

        try {
            const res = await callApi.post(`Carts/add_to_cart/`, req);
            if(res.status === 200){
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
        
    },

    getCartByUsername : async () : Promise<CartDetails[] | null> => {

        try {
            const res = await callApi.get(`Carts/`, {timeout: 10000});
            if(res.status === 200){
                return res.data;
            }else {
                return null;
            }
        } catch (error) {
            console.log(error);
            return null;
        }

    },

    getCartByUsernameV2 : async (username: string) : Promise<CartDetails[] | null> => {
        try {
            const res = await callApi.get(`Carts/from_username/${username}/`);
            if(res.status === 200){
                return res.data;
            }else {
                return null;
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    updateChoosedItems : async (selectedKeys: React.Key[]) : Promise<CartItems[]> => {

        try {
            const res = await callApi.put(`Carts/choose_cart_V3/`, {id: selectedKeys});
            if(res.status === 200){
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    deleteItems : async (ids: number[]) : Promise<CartDetails[] | void> => {

        try {
            const res = await callApi.delete('Carts/delete_cart_list/', {data : {product_ids: ids}});
            if(res.status === 200){
                return res.data;
            }else{
                return;
            }
        } catch (error) {
            console.log(error);
            return;
        }
    },
}