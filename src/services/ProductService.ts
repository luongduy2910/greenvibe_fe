import callApi from "@/lib/axios"
import { Product } from "@/types/products";
export const ProductService = {
    
    getRandomProducts: async () : Promise<Product[]> => {
        try {
            const res = await callApi.get("Products/get_all_products_random/");
            if(res.status === 200 && res.data !== null){
                return res.data;
            }else {
                return [];
            }
            
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    getFlashSaleProducts : async () : Promise<Product[]> => {
        try {
            const res = await callApi.get("Products/get_all_products_random_from_day/");
            if(res.status === 200 && res.data !== null){
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    getProductsByCategory : async (categoryName: string) : Promise<Product[]> => {
        
        try {
            const res = await callApi.get(`Products/${categoryName}/`);
            if(res.status === 200 && res.data !== null) {
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
        
    },

    getAllProducts : async () : Promise<Product[]> => {

        try {
            const res = await callApi.get("Products/");
            if(res.status === 200) {
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    changeStatusProductsto0 : async (productIds: number[]) : Promise<void> => {
        try {
            const res = await callApi.put("Products/change_status_products_to_0", {product_ids: productIds});
            if (res.status === 200){
                return;
            }
        } catch (error) {
            console.log(error);
            return;
        }
    },
}