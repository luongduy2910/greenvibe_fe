import callApi from "@/lib/axios"
import axios from "axios";

export const SearchService = {
    getListSuggestions: async (): Promise<Suggestion[]> => {
        try {
            const res = await axios.get("http://localhost:8080/suggestion");
            if(res.status === 200 && res.data != null){
                return res.data;
            }else{
                return [];
            }
            
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    search: async (keyword:string) : Promise<Suggestion[]> => {
        try {
            const res = await axios.get(`http://localhost:8080/suggestion/search?q=${keyword}`);
            if(res.status === 200){
                return res.data;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },
    insertOrUpdate: async(keyword: string): Promise<void> => {
        try {
            const res = await axios.post(`http://localhost:8080/suggestion?keywords=${keyword}`);
            if(res.status === 200){
                return;
            }
        } catch (error) {
            console.log(error);
            return;
        }
    }
};