import callApi from '@/lib/axios'
import {
  OrderDetailsCash,
  OrderDetailsVNPay,
  OrderReq,
  VNPaymentStatus,
} from '@/types/order'

export const OrderService = {

  createOrderByPaymentMethod: async (
    req: OrderReq,
  ): Promise<OrderDetailsCash | OrderDetailsVNPay | null> => {
    try {
      const res = await callApi.post('bill/create_bill_VNPAY/', req)
      if (res.status === 201) {
        return res.data
      } else {
        return null
      }
    } catch (error) {
      console.log(error)
      return null
    }
  },

  checkVNPayStatus : async (orderId: number) : Promise<VNPaymentStatus | null> => {
    try {
        const res = await callApi.get(`VNPAY/check_payment_status_v2/?order_id=${orderId}`);
        if(res.status === 200){
            return res.data;
        }else {
            return null;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
  },

  changeOrderStatus : async (orderId: number) : Promise<void> => {
    try {
      const res = await callApi.put(`bill/${orderId}`);
      if(res.status === 200){
        console.log("Cập nhật trạng thái thành công");
      }else {
        console.log("Cập nhật trạng thái thất bại");
      }
      
    } catch (error) {
      console.log(error);
    }
  }
}
