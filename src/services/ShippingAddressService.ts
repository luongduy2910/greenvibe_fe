import { thirdServiceCallApi } from "@/lib/axios";
import { AddressUser, District, InsertAddress, Province, Ward } from "@/types/address";
import callApi from "@/lib/axios"


export const ShippingAddressService = {


    insertAddress: async (newAddress : InsertAddress, username: string) : Promise<AddressUser[] | []> => {
        try {
            const res = await callApi.post(`address/add/${username}/`, newAddress);
            if(res.status === 200){
                return res.data?.addresses;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    updateAddress: async (updateAddress: InsertAddress, username: string, addressId: number) : Promise<AddressUser[] | []> => {
        try {
            const res = await callApi.put(`address/update/${username}/${addressId}/`, updateAddress);
            if(res.status === 200){
                return res.data?.addresses;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    deleteAddress: async(username: string, addressId: number) : Promise<AddressUser[] | []> => {

        try {
            const res = await callApi.delete(`address/delete/${username}/${addressId}/`);
            if(res.status === 200){
                return res.data?.addresses;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    // gọi api từ bên thứ 3 

    getProvince: async () : Promise<Province[]> => {

        try {
            const res = await thirdServiceCallApi.get("/api/province/");
            if(res.status === 200) {
                return res.data?.results;
            }else {
                return [];
            }
            
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    getDistrict: async (province_id: string) : Promise<District[]> => {
        try {
            const res = await thirdServiceCallApi.get(`api/province/district/${province_id}`);
            if(res.status === 200){
                return res.data?.results;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    },

    getWard: async (district_id:string) : Promise<Ward[]> => {
        try {
            const res = await thirdServiceCallApi.get(`api/province/ward/${district_id}`);
            if(res.status === 200){
                return res.data?.results;
            }else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    }
};