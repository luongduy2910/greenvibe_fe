import callApi from "@/lib/axios"
import { PaymentResponse } from "@/types";

export const PaymentService = {

    processPayment: async (amount: Number): Promise<void> => {
        try {
            const bodyReq = {
                amount
            };

            const res = await callApi.post("create_payment/", bodyReq, {});

            const data : PaymentResponse = res?.data;


            res.status === 200 && data.payment_url != null 
                ? window.open(data.payment_url, "_blank")
                : window.location.href = "/payment-error";
            
        } catch (error) {

            console.log(error);
            window.location.href = "/payment-error";
            
        }
    },
}