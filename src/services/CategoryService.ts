import { Category } from "@/types/category";
import callApi from "@/lib/axios";

export const CategoryService = {
    getAllCategories: async(): Promise<Category[]> => {
        try {
            const res = await callApi.get("categories/sub_categories/?is_sub=false");
            if(res.status === 200 && res?.data != null){
                return res.data;
            }else{
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    }
};