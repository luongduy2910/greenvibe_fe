import { UserInfo } from "@/types/userInfo";
import callApi from "@/lib/axios"

export const UserService = {

    getUserInfo : async( username: string ) : Promise<UserInfo | null> => {

        try {
            const res = await callApi.get(`users_v1/get_username/${username}/`);
            if(res.status === 200){
                return res.data;
            }else {
                return null;
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    },
    
}