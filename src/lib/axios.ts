import axios, { Axios } from "axios";

const baseURL = process.env.NEXT_PUBLIC_BASE_URL_TESTING;

const baseURL_Dev = process.env.NEXT_PUBLIC_BASE_URL_TESTING_DEV;

const baseMimicURL = process.env.NEXT_PUBLIC_MIMIC_BASE_URL;

const thirdServBaseURL = process.env.NEXT_PUBLIC_THIRD_SERVICE_BASE_URL;

const instance : Axios = axios.create({

    // có thể override baseurl 
    baseURL: baseURL,
    
    // có thể override default timeout 
    timeout: 10000,

    // basic auth dùng để truy cập các nguồn tài nguyên của admin 
    auth: {
        username: "greenvibe_store",
        password: "greenvibe_leehanshin_1235789.@"
    },

    // có thể override default header 
    headers: {
        "Content-Type": "application/json",
    },

});

export default instance;

export const thirdServiceCallApi : Axios = axios.create({

    baseURL: thirdServBaseURL,

    timeout: 10000,

    // có thể override default header 
    headers: {
        "Content-Type": "application/json",
    },

})