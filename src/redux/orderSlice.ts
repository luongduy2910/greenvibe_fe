import { OrderService } from '@/services/OrderService'
import {
  OrderDetailsCash,
  OrderDetailsVNPay,
  OrderReq,
  VNPayOrderDetails,
} from '@/types/order'
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

type InitialState = {
  isLoading: boolean
  orderDetails: OrderDetailsVNPay | OrderDetailsCash | null
  productIds: number[]
  idProductUsers: number[]
  vnpayBill: VNPayOrderDetails | null
}

const initialState: InitialState = {
  isLoading: false,
  orderDetails: null,
  productIds: [],
  idProductUsers: [],
  vnpayBill: null,
}

const orderSlice = createSlice({
  name: 'orderSlice',
  initialState,
  reducers: {
    setChosenItems: (state, action) => {
      // nếu mảng danh sách sản phẩm đã chọn có dữ liệu -> update store
      state.productIds = action.payload
    },

    setProductIds: (state, action) => {
      state.productIds = action.payload
    },

    setIdProductUsers: (state, action) => {
      state.idProductUsers = action.payload
    },

    setOrderDetails : (state, action) => {
      state.orderDetails = action.payload;
    }
  },
  extraReducers(builder) {
      builder.addCase(createOrderByPaymentMethod.fulfilled, (state, action) => {
        if (action.payload !== null) {
          state.orderDetails = action.payload
          localStorage.setItem('order_id', String(action.payload.id_bill))
          // localStorage.setItem('order_details', JSON.stringify(action.payload))
          if (action.payload.chosen_items.length > 0) {
            state.productIds = action.payload.chosen_items
              .filter((item) => item.choose === true)
              .map((item) => item.product_id)

            state.idProductUsers = action.payload.chosen_items
              .filter((item) => item.choose === true)
              .map((item) => item.id_products_user)
          }
        }

        if(action.payload?.type === "1"){

          if ((action.payload as OrderDetailsVNPay).payment_url !== undefined) {
            window.open(
              (action.payload as OrderDetailsVNPay).payment_url,
              '_blank',
              'noopener,noreferrer',
            )
          }
        }
        
      })
  },
})

export const {
  setChosenItems,
  setProductIds,
  setIdProductUsers,
  setOrderDetails
} = orderSlice.actions

export const createOrderByPaymentMethod = createAsyncThunk(
  'createOrderByPaymentMethod',
  async (
    req: OrderReq,
  ): Promise<OrderDetailsCash | OrderDetailsVNPay | null > => {
    return await OrderService.createOrderByPaymentMethod(req)
  },
)

export default orderSlice.reducer
