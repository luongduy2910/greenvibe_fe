import { createSlice } from '@reduxjs/toolkit'

type InitialState = {
    isLoading: boolean
}

const initialState : InitialState = {
    isLoading: true,
}

const logoloadingSlice = createSlice({
  name: "logoloadingSlice",
  initialState,
  reducers: {

    toogleLoading : (state, action) => {
        state.isLoading = action.payload;
    },

  }
});

export const { toogleLoading } = logoloadingSlice.actions

export default logoloadingSlice.reducer