import { createSlice } from '@reduxjs/toolkit'

type InitialState = {
    expandItems : boolean
    paymentMethod: number,
}

const initialState : InitialState = {
    expandItems: false,
    paymentMethod: 0
}

const paymentSlice = createSlice({
  name: "paymentSlice",
  initialState,
  reducers: {

    toggleExpandItems : (state, action) => {
        state.expandItems = action.payload;
    },

    setPaymentMethod : (state, action) => {
      state.paymentMethod = action.payload
    }
  }
});

export const { toggleExpandItems, setPaymentMethod } = paymentSlice.actions

export default paymentSlice.reducer