import { createSlice } from '@reduxjs/toolkit';

interface OverlayState {
  showOverlay: boolean;
  isFetch: boolean;
}

const initialState: OverlayState = {
  showOverlay: false,
  isFetch: false,
};

const overlaySlice = createSlice({
  name: 'overlay',
  initialState,
  reducers: {
    setShowOverlay: (state, action) => {
      state.showOverlay = action.payload;
    },

    setIsFetch : (state, action) => {
      state.isFetch = action.payload;
    }
  },
});

export const { setShowOverlay, setIsFetch } = overlaySlice.actions;
export default overlaySlice.reducer;
