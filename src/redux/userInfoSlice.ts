import { UserService } from '@/services/UserService';
import { UserInfo } from '@/types/userInfo';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { setListAddress } from './addressSlice';

type InitialState = {

    userInfo: UserInfo | null
}


const initialState : InitialState = {
    userInfo: null
}

const userInfoSlice = createSlice({
  name: "userInfoSlice",
  initialState,
  reducers: {
  },

  extraReducers: (builder) => {
    builder.addCase(getUserInfo.fulfilled, (state, action) => {
        state.userInfo = action.payload;
    });
  }
});

export const getUserInfo = createAsyncThunk("getUserInfo", async (username: string, { dispatch }) : Promise<UserInfo | null> => {
    
    const userInfo = await UserService.getUserInfo(username);
    
    if(userInfo !== null){
      // sau khi lấy user info gồm có address -> gửi action để cập nhật address 
      dispatch(setListAddress(userInfo.addresses));
    }

    return userInfo;
    
});

export const {} = userInfoSlice.actions

export default userInfoSlice.reducer