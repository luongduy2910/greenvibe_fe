import { combineReducers, configureStore, Tuple } from "@reduxjs/toolkit";
import overlaySlice from "./overlaySlice";
import searchKeywordSlice from "./searchKeywordSlice";
import categorySlice from "./categorySlice";
import cartSlice from "./cartSlice";
import logoloadingSlice from "./logoloadingSlice"
import orderSlice from "./orderSlice";
import paymentSlice from "./paymentSlice";
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from "redux-persist";
import logger from 'redux-logger'
import { thunk } from "redux-thunk";
import addressSlice from "./addressSlice";
import userInfoSlice from "./userInfoSlice";


// cấu hình redux persist bằng cách sử dụng localstorage
const persistConfig = {
    key: 'root',
    storage,
}

const rootReducer = combineReducers({

    overlaySlice: overlaySlice,
    searchKeywordSlice: searchKeywordSlice,
    categorySlice: categorySlice,
    cartSlice: cartSlice,
    logoloadingSlice: logoloadingSlice,
    orderSlice: orderSlice,
    paymentSlice: paymentSlice,
    addressSlice: addressSlice,
    userInfoSlice: userInfoSlice,

})

const persistedReducer = persistReducer(persistConfig, rootReducer);



// cấu hình redux store 
export const store = configureStore({

    // liệt kê các reducer 
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger).concat(thunk),
    devTools: process.env.NODE_ENV !== "production",
})

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
