import { CartService } from '@/services/CartService';
import { AddToCart, CartDetails, CartItems } from '@/types/cart';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { message } from 'antd';
import React from 'react';

type InitialState = {
    cartDetails: CartDetails | null ,
    selectedKeys: number[],
    isLoading: boolean,
    totalPrice: number,
    isOpenCartDrawer: boolean; 
}

const initialState : InitialState = {
    cartDetails: null,
    selectedKeys: [],
    isLoading: false,
    totalPrice: 0,
    isOpenCartDrawer: false,
}

const updateSelectedKeys = (items: CartItems[]) : number[] => {
  const list : number[] = [];

  console.log("items :", items);
  for(let i = 0; i < items.length; i++){

    if(items[i].choose === true){
      list.push(items[i]?.id_products_user)
    }
  }
  return list;
};

const countTotalPrice = (items: CartItems[]) : number => {

  let totalPrice : number = 0;

  for(let i = 0; i < items?.length; i++){
    if(items[i]?.choose === true){

      totalPrice += items[i]?.price;
    }
  }

  return totalPrice;

}

const cartSlice = createSlice({
  name: "cartSlice",
  initialState,
  reducers: {
    setCartDetails : (state, action) => {
      if(action.payload != null && action.payload.length > 0){

        // nếu cart có dữ liệu -> update cart trong redux store
        state.cartDetails = action?.payload[0];

      }

      if(action?.payload != null && action?.payload[0]?.items?.length > 0) {

        state.selectedKeys = updateSelectedKeys(action?.payload[0]?.items);

        state.totalPrice = countTotalPrice(action?.payload[0]?.items);
      }

    },

    setSelectedKeys: (state, action) => {
      if(action?.payload != null && action?.payload.length > 0){
        state.selectedKeys = action.payload;
      }else{
        state.selectedKeys = [];
      }
    },

    setCartDrawer : (state, action) => {
      state.isOpenCartDrawer = action.payload;
    },

    setDefaultAfterOrderSucess : (state) => {
      state.selectedKeys = [];
      state.totalPrice = 0;
      state.isOpenCartDrawer = false;
    },

    updateCartAfterSuccessOrder: (state, action) => {
      if (state.cartDetails !== null) {
        
        state.cartDetails.items = action.payload[0].items;

      }
    },
  },

  extraReducers : (builder) => {

      builder.addCase(getCartDetailsV2.fulfilled, (state, action) => {
        
        if(action.payload !== null){

          state.cartDetails = action.payload[0];
          
        }

      })

      builder.addCase(addToCart.fulfilled, (state, action) => {
        if(action.payload.length > 0){
          state.cartDetails = action.payload[0];
          message.success("Thêm sản phẩm vào giỏ hàng thành công");
        }else{
          message.error("Thêm sản phẩm thất bại");
        }
      });

      builder.addCase(getCartDetails.fulfilled, (state, action) => {
        if(action.payload != null){
          state.cartDetails = action.payload[0];
        }
      });

      builder.addCase(updateChoosedItems.pending, (state, action) => {
        state.isLoading = true;
      });

      builder.addCase(updateChoosedItems.fulfilled, (state, action) => {
        // sau khi update thành công -> dựa trên dữ liệu trả về của be -> cập nhật lại selected keys
        state.selectedKeys = updateSelectedKeys(action.payload);
        state.totalPrice = countTotalPrice(action.payload);
        state.isLoading = false;
        if(state.cartDetails !== null){

          state.cartDetails.items = action.payload;
        }
        
      });

      builder.addCase(deleteItems.pending, (state, action) => {
        state.isLoading = true;
      });

      builder.addCase(deleteItems.fulfilled, (state, action) => {
        // sau khi delete thành công -> cập nhật lại store redux 
        if(action.payload != null || action.payload != undefined){
          state.cartDetails = action.payload[0];
          state.totalPrice = countTotalPrice(action.payload[0]?.items);
          state.isLoading = false;
          message.success("Xóa sản phẩm thành công");
        }else {
          state.isLoading = false;
          message.error("Xóa sản phẩm thất bại");
        }
        
      });

  },
});

export const addToCart = createAsyncThunk("addToCart", async (req: AddToCart) : Promise<CartDetails[]> => {
  return await CartService.addToCart(req);
});

export const getCartDetails = createAsyncThunk("getCartDetails", async () : Promise<CartDetails[] | null> => {
    return await CartService.getCartByUsername();
});

export const updateChoosedItems = createAsyncThunk("updateChoosedItems", async (selectedKeys: React.Key[]) : Promise<CartItems[]> => {
  return await CartService.updateChoosedItems(selectedKeys);
});

export const deleteItems = createAsyncThunk("deleteItems", async (ids: number[]) : Promise<CartDetails[] | void> => {
  return await CartService.deleteItems(ids);
});

export const getCartDetailsV2 = createAsyncThunk("getCartDetailsV2", async (username: string) : Promise<CartDetails[] | null> => {
  return await CartService.getCartByUsernameV2(username);
});

export const { setCartDetails, setSelectedKeys, setCartDrawer, setDefaultAfterOrderSucess, updateCartAfterSuccessOrder } = cartSlice.actions;

export default cartSlice.reducer