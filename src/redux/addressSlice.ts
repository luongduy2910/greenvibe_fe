import { ShippingAddressService } from '@/services/ShippingAddressService';
import {  AddressUser, DeleteAddressPayload, InsertAddressPayload, UpdateAddressPayload } from '@/types/address';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { message } from 'antd';

type InitialState = {
    listAddress: AddressUser[],
    chooseAddress: number,
    insertOrUpdate: number,
}
const initialState : InitialState = {
    
    listAddress: [],
    chooseAddress: -1,
    insertOrUpdate: 0,
}

const addressSlice = createSlice({
  name: "addressSlice",
  initialState,
  reducers: {

    setListAddress: (state, action) => {

        state.listAddress = action.payload;

        if(action.payload != null && state.chooseAddress === -1) {

          state.chooseAddress = action.payload?.filter((item: any) => item.status === 1)[0]?.id;

        }

    },

    setChooseAddress : (state, action) => {

      state.chooseAddress = action.payload;
    },

    setInsertOrUpdate : (state, action) => {

      state.insertOrUpdate = action.payload;
    },

    checkDelete : (state, action) => {

      if (state.chooseAddress === action.payload?.address_id) {

        // nếu địa chỉ xóa trùng với địa chỉ đã chọn ban đầu -> lấy địa chỉ mặc định 
        const defaultAdddess = action.payload?.addresses?.filter((item : any) => item.status === 1);

        // cập nhật địa chỉ giao hàng là mặc định 
        state.chooseAddress = defaultAdddess[0].id;
      }
    }
  },

  extraReducers(builder) {
      builder.addCase(insertAddress.fulfilled, (state, action) => {
        if(action.payload.length === 0){

          // nếu độ dài mảng bằng 0 => call api bị lỗi không cập nhật list address 
          message.error("Thêm địa chỉ mới thất bại");
        }
        else {

          // trường hợp còn lại -> thêm địa chỉ thành công -> cập nhật list address
          state.listAddress = action.payload;
          message.success("Thêm địa chỉ mới thành công");

        }
        
      })

      builder.addCase(updateAddress.fulfilled, (state, action) => {

        if(action.payload.length === 0){

          message.error("Cập nhật địa chỉ thất bại");
        }else {

          state.listAddress = action.payload;
          message.success("Cập nhật địa chỉ thành công");

        }
        
      })

      builder.addCase(deleteAddress.fulfilled, (state, action) => {

        if(action.payload.length === 0){

          message.error("Xóa địa chỉ thất bại");

        }else {

          state.listAddress = action.payload;
          message.success("Xóa địa chỉ thành công");

          // kiểm tra xem địa chỉ xóa có phải là địa chỉ ban đầu mà user chọn hay không 


        }
        
      })


  },
});

export const { setListAddress, setChooseAddress, setInsertOrUpdate, checkDelete } = addressSlice.actions

export const insertAddress = createAsyncThunk("insertAddress", async ( payload: InsertAddressPayload ): Promise<AddressUser[] | []> => {
  
  return await ShippingAddressService.insertAddress(payload.reqBody , payload.username);
  
});

export const updateAddress = createAsyncThunk("updateAddress", async (payload: UpdateAddressPayload) : Promise<AddressUser[] | []> => {

  return await ShippingAddressService.updateAddress(payload.reqBody, payload.username, payload.address_id);

});

export const deleteAddress = createAsyncThunk("deleteAddress", async(paload: DeleteAddressPayload, {dispatch}) : Promise<AddressUser[] | []> => {
   const result = await ShippingAddressService.deleteAddress(paload.username, paload.address_id);
   const deletePayload = {
    addresses: result,
    address_id: paload.address_id
   };

   dispatch(checkDelete(deletePayload));

   return result;
   
})



export default addressSlice.reducer