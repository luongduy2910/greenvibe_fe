import { Category } from '@/types/category';
import { createSlice } from '@reduxjs/toolkit'

type InitialState = {

    categories: Category[],
    subCategories: SubCategory[],

}

const initialState : InitialState = {
    categories : [],
    subCategories: [],
}

const categorySlice = createSlice({
  name: "categorySlice",
  initialState,
  reducers: {

    setCategories : (state, action) => {

        state.categories = action.payload;
        
    },

    setSubCategories: (state, action) => {

      state.subCategories = action.payload;

    },

  }
});

export const { setCategories, setSubCategories } = categorySlice.actions;

export default categorySlice.reducer;