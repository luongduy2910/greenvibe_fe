import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { SearchService } from '@/services/SearchService';

type InitialState = {

    listSuggestions: Suggestion[],
    itemToShow: number,
    expanded: boolean,
    isFocused: boolean,
    isSearching: boolean,

};

const initialState : InitialState = {
    listSuggestions: [],
    itemToShow: 2,
    expanded: false,
    isFocused: false,
    isSearching: false,
}

const searchKeywordSlice = createSlice({
  name: "searchKeywordSlice",
  initialState,
  reducers: {

    setIntialListSuggestions: (state, action) => {
      state.listSuggestions = action.payload;
    },
    
    setItemToShow: (state, action) => {
      if(action.payload === 2){
        state.itemToShow = action.payload;
        state.expanded = false;
      }else{
        state.itemToShow = action.payload;
        state.expanded = true;
      }
    },

    setIsFocused: (state, action) => {
      state.isFocused = action.payload;
    },

    setIsSearching : (state, action) => {
      state.isSearching = action.payload;
    },

  },

  extraReducers: (buidler) => {
    
    buidler.addCase(setByKeywords.fulfilled, (state, action) => {
        
        // cập nhật lại suggestions chỉ khi có dữ liệu 
        // nếu không có dữ liệu -> không cập nhật -> dữ lại store gọi ý cũ 
        if(action.payload.length > 0){
          state.listSuggestions = action.payload;
        }

        // state.listSuggestions = action.payload;

        // lúc này đang ở chế độ search -> hiển thị full danh sách suggestions 
        state.itemToShow = action.payload?.length;
        
    });

    buidler.addCase(getSuggestions.fulfilled, (state, action) => {
      if(action.payload !== null){

        state.listSuggestions = action.payload;

      }
    });
    
  }

});


export const setByKeywords = createAsyncThunk("setByKeyword", async ( keywords: string ) => {
    return await SearchService.search(keywords);
});

export const getSuggestions = createAsyncThunk('getSuggestions', async () : Promise<Suggestion[]> => {
  return await SearchService.getListSuggestions();
});

export const { setIntialListSuggestions, setItemToShow, setIsFocused, setIsSearching } = searchKeywordSlice.actions;

export default searchKeywordSlice.reducer;