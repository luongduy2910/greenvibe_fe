import { NextResponse } from 'next/server';

export function middleware(request: Request) {

  // thay đổi default header bằng header custom 
  const requestHeaders = new Headers(request.headers);

  // trong header thêm request url (để trong root layout sử dụng render có điều kiện)
  requestHeaders.set('x-url', request.url);

  return NextResponse.next({
    request: {
      // Apply new request headers
      headers: requestHeaders,
    }
  });
}
