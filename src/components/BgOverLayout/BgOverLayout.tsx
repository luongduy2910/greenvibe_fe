'use client'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { setShowOverlay } from '@/redux/overlaySlice'
import { setIsFocused } from '@/redux/searchKeywordSlice'

export function BgOverLayout() {
  const showOverlay = useAppSelector((state) => state.overlaySlice.showOverlay)
  const dispatch = useAppDispatch()
  return (
    <>
      {showOverlay && (
        <div
          onClick={() => {

            // khi click ra bên ngoài -> mất nền và tắt thẻ div search content 
            dispatch(setShowOverlay(false));
            dispatch(setIsFocused(false));
            
          }}
          className="bg__overlay"
        />
      )}
    </>
  )
}
