'use client'
import {
  UserOutlined,
  ContainerOutlined,
  SendOutlined,
} from '@ant-design/icons'
import Link from 'next/link'
import React from 'react'
import { usePathname } from 'next/navigation'

export function ProfileCategorySideBar() {
  const pathname = usePathname()
  // xây dựng dữ liệu cần thiết để dựng sidebar trong profile page
  const dataProfile = [
    {
      label: 'Thông tin tài khoản',
      link: '/customer/profile',
      icon: <UserOutlined style={{ fontSize: '20px' }} />,
    },

    {
      label: 'Đơn hàng của tôi',
      link: '/customer/order/history',
      icon: <ContainerOutlined style={{ fontSize: '20px' }} />,
    },

    {
      label: 'Sổ địa chỉ',
      link: '/customer/address',
      icon: <SendOutlined style={{ fontSize: '20px' }} />,
    },
  ]

  const renderItems = dataProfile.map((item, index) => {
    return (
      <li
        key={index}
        className={`px-2 py-3 ${
          pathname === item.link ? 'bg-gray-200 border border-gray-100' : ''
        } hover:bg-gray-200 transition-all hover:border hover:border-gray-100 flex flex-row gap-x-3`}
      >
        {item.icon}
        <Link className="text-lg text-gray-700" href={item.link}>
          {item.label}
        </Link>
      </li>
    )
  })
  return (
    <ul className="profile__category flex flex-col gap-y-5">{renderItems}</ul>
  )
}
