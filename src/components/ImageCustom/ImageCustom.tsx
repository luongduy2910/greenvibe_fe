import Image from 'next/image'
import React from 'react'

type Props = {
    link : string
    width?: number
    height?: number
    alt: string
    isResized?: boolean
}

export function ImageCustom({ link, width, height, alt, isResized } : Props) {
  return (
    <Image width={width} height={height} src={process.env.NEXT_PUBLIC_BASE_URL_IMG + link} alt={alt} className={isResized ? 'w-full h-full' : ''}/>
  )
}
