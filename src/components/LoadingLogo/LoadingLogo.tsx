'use client'
import { useAppSelector } from '@/redux/hooks'
import React from 'react'

export function LoadingLogo() {
  const { isLoading } = useAppSelector((state) => state.logoloadingSlice)
  return (
    <>
      {isLoading && (
        <>
          <div className={`loading-screen` }>
            {isLoading && (
              <video autoPlay loop muted className="loading-video">
                <source src="/videos/4k.mp4" type="video/mp4" />
                Trình duyệt của bạn không hỗ trợ video.
              </video>
            )}
          </div>
        </>
      )}
    </>
  )
}
