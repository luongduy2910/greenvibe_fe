'use client'
import { useAppSelector } from '@/redux/hooks'
import Link from 'next/link';
import React from 'react'
import { ImageCustom } from '../ImageCustom/ImageCustom';

export function CategorySideBar() {

  const categories = useAppSelector((state) => state.categorySlice.categories);

  const renderCategories = () => {
    return categories?.map((category, index) => {
      return (
        <Link className='flex flex-row items-center gap-x-5' key={index} href={`/category/${category.name_category}`}>
          <ImageCustom width={30} height={30} link={category.image} alt={category.name_category} />
          <span className='menu__items hover:font-bold text-lg font-medium text-gray-700'>{category.name_category}</span>
        </Link>
      )
    });
  }

  return (

    <div className='category__side__bar shadow-2xl bg-white p-10 rounded-xl w-full relative'>
      <span className='text-lg font-bold'>Danh mục</span>
      <ul className='category__list__side__bar mt-10 flex flex-col gap-y-10'>
        {renderCategories()}
      </ul>
    </div>

  ) 
  
}
