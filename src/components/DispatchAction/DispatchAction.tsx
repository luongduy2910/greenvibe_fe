'use client'
import { getCartDetailsV2, setCartDetails } from '@/redux/cartSlice'
import { setCategories, setSubCategories } from '@/redux/categorySlice'
import { toogleLoading } from '@/redux/logoloadingSlice'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { CartDetails } from '@/types/cart'
import { Category } from '@/types/category'
import React, { useRef } from 'react'
import { setIntialListSuggestions } from '@/redux/searchKeywordSlice'
import { setOrderDetails } from '@/redux/orderSlice'
import { Address } from '@/types/address'
import { setListAddress } from '@/redux/addressSlice'
import { getUserInfo } from '@/redux/userInfoSlice'
import { setIsFetch } from '@/redux/overlaySlice'

type Props = {
  categories: Category[],
  // suggestions: Suggestion[],
  subCategories: SubCategory[],
}

export function DispatchAction({ categories, subCategories }: Props) {

  const hasRun = useRef(false);

  const dispatch = useAppDispatch();

  const { isFetch } = useAppSelector(state => state.overlaySlice);

  // sau khi có chức năng login -> bóc username tại đây để lấy danh sách cart và user info 
  const USERNAME = "greenvibe_store_test_v2";
  
  // sau khi nhận dữ liệu từ root layout -> gửi data lên redux store 

  dispatch(setCategories(categories));

  dispatch(setSubCategories(subCategories));

  // dispatch(getCartDetailsV2(USERNAME));

  // dispatch(getUserInfo(USERNAME))

  // dispatch(setIntialListSuggestions(suggestions));

  const fetchAsyncApi = async () => {

    // những api cần gọi khi vào trang chủ (trường hợp user đã đăng nhập)

    await dispatch(getCartDetailsV2(USERNAME));

    await dispatch(getUserInfo(USERNAME));

    // sau khi fetch api thành công -> set biến isFetch để không call api liên tục 

    dispatch(setIsFetch(true));
  };

  // dispatch(setListAddress(addresses));

  // if(typeof window !== 'undefined'){
    
  //   dispatch(setOrderDetails(JSON.parse(localStorage.getItem('order_details')!)));
  // }


  React.useEffect(() => {
    if(!hasRun.current){

      hasRun.current = true;
      
      setTimeout(() => {
        dispatch(toogleLoading(false));
      }, 6000);
      
      if(!isFetch) {
        
        fetchAsyncApi();

      }
    }
    
  },[]);
  

  // kết thúc ----------------------------------------------------------------

  return <></>
}
