import React from 'react'
import { HeaderBrand } from '../HeaderCustom/NavBarTop/HeaderBrand'

export function HeaderPayment() {
  return (
    <section className='nav__bar__payment bg-white border border-gray-100 shadow-md py-5'>
        {/* navbar payment bao gồm 2 component chính : brand payment và thông tin website  */}
        <nav className='nav__bar__wrapper w-[80%] mx-auto p-2 flex flex-row justify-between items-center'>
            <div className='nav__bar__brand__payment flex flex-row gap-4 items-center'>
                <div className='payment__brand'>
                    <HeaderBrand/>
                </div>
                <div className='payment__title w-[41%] border-l-2 border-[#53D260] pl-2'>
                    <h2 className='text-[#53D260] text-3xl font-semibold'>
                        Thanh Toán
                    </h2>
                </div>
            </div>
            <div className='nav__bar__company__info'>
                <span>Company info</span>
            </div>
        </nav>

    </section>
  )
}
