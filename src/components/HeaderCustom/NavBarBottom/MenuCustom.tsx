import { Category } from '@/types/category'
import Link from 'next/link'
import React from 'react'

type Props = {
  subCategories: SubCategory[]
}

export function MenuCustom({ subCategories }: Props) {

  const renderCategoryMenu = () => {
    return subCategories?.map((subCategory, index) => {
      return (
        <Link
          key={index}
          href={`/category/${subCategory.name_category}`}
        >
          <span className='menu__items text-xl text-gray-500 hover:text-gray-700 hover:font-semibold inline-block'>
            {subCategory?.name_category}
          </span>
        </Link>
      )
    })
  }
  
  return (
    <ul className="menu__category flex items-center justify-between">
      {renderCategoryMenu()}
    </ul>
  )
}
