"use client"
import React from "react";
import { SearchCustom } from "./NavBarTop/SearchCustom";
import { HeaderUserInfo } from "./NavBarTop/HeaderUserInfo";
import { Category } from "@/types/category";
import { MenuCustom } from "./NavBarBottom/MenuCustom";
import { HeaderBrand } from "./NavBarTop/HeaderBrand";
import { useAppSelector } from "@/redux/hooks";


// type Props = {

//   categoryList: Category[],
//   subCategoryList: SubCategory[],
//   suggestionList: Suggestion[],

// }

export function HeaderCustom() {

  const { categories: categoryList, subCategories: subCategoryList } = useAppSelector(state => state.categorySlice);

  const { listSuggestions: suggestionList } = useAppSelector(state => state.searchKeywordSlice);

  
  return (
    <section className="nav__bar bg-white shadow-xl p-5 z-[999] relative w-full h-full">
      {/* Trong phần navbar chia làm 2 component: navbar top and botton  */}
      <nav className="w-4/5 mx-auto flex flex-col gap-y-10">

        {/* Trong phần navbar top  chia làm 3 component  */}
        <div className="w-full nav__bar__top flex flex-row items-center justify-between">
          
          {/* component1: header brand + logo  */}
          <div className="nav__brand w-1/5">
            <HeaderBrand/>
          </div>

          {/* component2: header search  */}
          <div className="nav__search w-3/5">
            <SearchCustom categories={categoryList} suggestions={suggestionList}/>
          </div>

          {/* component3: header logic user info  */}
          <div className="nav__userinfo">
            <div className="nav__user__info w-full">
              <HeaderUserInfo/>
            </div>          
          </div>

        </div>

        {/* Trong phần navbar bottom có category list  */}
        <div className="w-full nav__bar__bottom">
          <MenuCustom subCategories={subCategoryList}/>
        </div>

      </nav>

    </section>
  )
};