'use client'
import { Input, ConfigProvider } from 'antd'
import React, { ChangeEvent } from 'react'
import { SearchOutlined } from '@ant-design/icons'
import Link from 'next/link'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { setShowOverlay } from '@/redux/overlaySlice'
import {
  setByKeywords,
  setIntialListSuggestions,
  setItemToShow,
  setIsFocused,
  setIsSearching,
  getSuggestions,
} from '@/redux/searchKeywordSlice'
import { SearchProps } from 'antd/es/input'
import { SearchService } from '@/services/SearchService'
import { useRouter } from 'next/navigation'
import { Category } from '@/types/category'
import { ImageCustom } from '@/components/ImageCustom/ImageCustom'
import { ArrowDownOutlined, ArrowUpOutlined, RiseOutlined, ShopOutlined } from '@ant-design/icons'

type Props = {
  suggestions: Suggestion[]
  categories: Category[]
}

export function SearchCustom({ suggestions, categories }: Props) {
  console.log(categories)

  // sử dụng dispatch để call action trong redux
  const dispatch = useAppDispatch()

  // bóc tách các state từ redux slice 
  const {
    listSuggestions,
    itemToShow,
    expanded,
    isFocused,
    isSearching,
  } = useAppSelector((state) => state.searchKeywordSlice)

  console.log('isFocused', isFocused)

  // bóc search component từ input
  const { Search } = Input

  const router = useRouter()

  // gọi api search tìm kiếm
  const searchKeywords = async (event: ChangeEvent<HTMLInputElement>) => {
    let keywords = event.target.value
    if (keywords !== '') {
      // nếu đang search -> chuyển về chế độ full gợi ý
      dispatch(setIsSearching(true))

      // call async thunk api
      dispatch(setByKeywords(keywords))
    } else {
      // nếu keywords rỗng -> chuyển về chế độ bình thường
      dispatch(setIsSearching(false))

      // dispatch(setIntialListSuggestions(suggestions))
      dispatch(setItemToShow(2))
    }
  }

  // trường hợp user nhập keyword không có trong danh sách tìm kiếm -> bấm nút search
  const onSearch: SearchProps['onSearch'] = async (keywords, _e, info) => {
    console.log(info?.source, keywords)

    if (keywords !== '') {

      // nếu là từ khóa mới -> insert từ khóa vào database
      await SearchService.insertOrUpdate(keywords)

      // tắt nền background trước khi chuyển trang kết quả tìm kiếm
      dispatch(setShowOverlay(false))

      // tắt thẻ gợi ý tìm kiếm trước khi chuyển trang kết quả tìm kiếm
      dispatch(setIsFocused(false))

      // Trước khi chuyển trang -> cập nhật lại suggestions để hiển thị từ khóa search mới nhất 
      await dispatch(getSuggestions());

      // sau khi thêm -> điều hướng trang kết quả tìm kiếm
      router.push(`/search?q=${keywords}`)

      // sau khi search xong -> set các state về lại default  
      setTimeout(() => {
        dispatch(setItemToShow(2));
        dispatch(setIsSearching(false));
      }, 200); 

    }
  }

  // render các từ khóa nổi bật
  const renderListSuggestions = () => {
    return listSuggestions?.slice(0, itemToShow).map((suggestion, index) => {
      return (
        <li
          onClick={() => handleMoveSearchPage(suggestion.title)}
          className="flex gap-2 items-center bg-white hover:bg-gray-300 transition hover:border hover:rounded-lg p-2 cursor-pointer"
          key={index}
        >
          <SearchOutlined />
          <span className="text-black font-semibold text-lg">
            {suggestion?.title}
          </span>
        </li>
      )
    })
  }

  // render các danh mục nổi bật
  const renderListCategories = () => {
    return categories?.slice(0, 8).map((category, index) => {
      return (
        <Link
          className="category__item flex flex-col gap-y-2 p-4 justify-center items-center bg-transparent hover:shadow-2xl hover:border hover:border-gray-100 rounded-lg"
          key={index}
          href={`/category/${category.name_category}`}
        >
          <ImageCustom
            width={50}
            height={50}
            link={category.image}
            alt={category.name_category}
          />
          <span className="text-[15px] font-normal text-gray-400">
            {category.name_category}
          </span>
        </Link>
      )
    })
  }

  // xây dựng toggle "show more - show less"
  const showMore = () => {
    // khi user click vào toggle -> mất tính năng onFocus trên thanh input -> bỏ onBlur function 

    // nếu item = 2 -> hiển thị thêm -> gửi action để set lại item bằng độ dài của mảng sugggestions -> set biến expanded là true
    // nếu item != 2 -> rút gọn lại -> gửi action để set lại item về lại giá trị default là 2 -> set biến expanded là false
    itemToShow === 2
      ? dispatch(setItemToShow(suggestions?.length))
      : dispatch(setItemToShow(2))
  }

  // khi user on focus vào thanh search 
  const handleOnFocus = () => {

    // gửi action để bật nền bg với độ trễ 100ms 
    setTimeout(() =>  dispatch(setShowOverlay(true)) , 100);

    // gửi action vào state isFocused để hiển thị thẻ div search__content với độ trễ 100ms
    setTimeout(() => dispatch(setIsFocused(true)), 100);

    // gửi action để cập nhật state listSuggestions bằng dữ liệu đã fetch với độ trễ 150ms
    // setTimeout(() => dispatch(setIntialListSuggestions(suggestions)), 150); 
   
  };

  React.useEffect(() => {

    // mỗi lần về lại trang -> set lại default suggestions
    dispatch(setIntialListSuggestions(suggestions));

  }, [suggestions]);


  // khi user click vào gợi ý tìm kiếm -> chuyển trang
  const handleMoveSearchPage = async ( keywords: string ) => {

    // ẩn search__content div 
    dispatch(setIsFocused(false));

    // tắt nền bg 
    dispatch(setShowOverlay(false));

    // gọi api để cập nhật số lượt user click vào gợi ý 
    await SearchService.insertOrUpdate(keywords);

    // điều hướng người dùng qua trang kết quả tìm kiếm sau 100ms 
    setTimeout(() => router.push(`/search?q=${keywords}`), 100);

  };
  

  return (
    <div className="relative header__search">
      {/* Cấu hình default theme -> chỉ sử dụng trong scope này  */}
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: '#53D260',
          },
        }}
      >
        {/* Component search  */}
        <Search
          placeholder="tìm kiếm sản phẩm ..."
          onChange={(e) => searchKeywords(e)}
          enterButton
          size="large"
          onFocus={handleOnFocus}
          onSearch={onSearch}
        />

        {/* Khi người dùng nhấp vào thanh search -> hiển thị phần danh sách các từ khóa gợi ý search  */}

        {/* search content bao gồm 3 components  */}
        {isFocused && (
          <div className="search__content w-full bg-white rounded-xl p-10 shadow-lg absolute left-0 flex flex-col gap-y-4">

            {/* component 1: danh sách các từ khóa gợi ý  */}
            <div className="search__suggestion w-full">
              <span className="text-3xl font-bold flex flex-row gap-x-4 items-center justify-center">
                <span className='title'>
                  Từ khóa nổi bật
                </span>
                <RiseOutlined style={{color : "#53D260", fontSize: '30px'}} />
              </span>
              <ul className="list__suggestion flex flex-col gap-y-3 mt-5">
                {renderListSuggestions()}
              </ul>
            </div>

            {/* Khi người dùng đang không tìm kiếm -> hiển thị toggle show-more-less và các danh mục nổi bật  */}
            {!isSearching && (
              <>

                {/* component2: nút show-more-less toggle  */}
                <div className="show__more__less flex justify-center items-center">
                  <span
                    onClick={showMore}
                    className="text-blue-400 text-lg bg-transparent hover:shadow-xl rounded-lg hover:border transition hover:border-gray-300 p-3 cursor-pointer"
                  >
                    {expanded ? 'Thu gọn' : 'Xem thêm'}
                    {expanded ? <ArrowUpOutlined /> : <ArrowDownOutlined />}
                  </span>
                </div>

                {/* component3: các danh mục nổi bật  */}
                <div className="search__categories w-full border-t border-gray-200 py-5">
                  <span className="text-3xl font-bold flex flex-row gap-x-4 items-center justify-center">
                    <span className='title'>
                      Danh mục nổi bật
                    </span>
                    <ShopOutlined style={{color : "#53D260", fontSize: '30px'}} />
                  </span>
                  <ul className="list__category grid grid-cols-4 gap-5 mt-5">
                    {renderListCategories()}
                  </ul>
                </div>

              </>
            )}
          </div>
        )}
      </ConfigProvider>
    </div>
  )
}
