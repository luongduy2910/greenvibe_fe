'use client'
import React from 'react'
import LogoHeader from "../../../../public/images/logo_header.png";
import Image from 'next/image';
import Icon from "../../../../public/images/logo_success-15.png"
import { useRouter } from 'next/navigation';

export function HeaderBrand() {
  const router = useRouter();
  const handleBackToHome = () => {
    router.push("/");
  }
  return (
    <div onClick={handleBackToHome} className='header__brand w-full flex flex-row items-center gap-1 cursor-pointer'>
        <Image className="nav__logo" src={LogoHeader} alt="logo_img" width={50} height={50}/>
        <h1 className="nav__brand__title text-4xl font-extrabold base__color">GREENVIBE</h1>
        <Image src={Icon} width={50} height={50} alt="logo" className="animate-bounce transition-all"/>
    </div>
  )
}
