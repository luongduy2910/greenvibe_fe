'use client'
import Link from 'next/link'
import React from 'react'
import { UserOutlined } from '@ant-design/icons'
import { useAppSelector } from '@/redux/hooks'
import { SmileOutlined } from '@ant-design/icons'
import { ConfigProvider, Dropdown, MenuProps, Space } from 'antd'

export function HeaderUserInfo() {

  // xây dựng các items trong dropdown tài khoản người dùng 
  const items : MenuProps['items'] = [
    {
      label: <Link className='p-5 text-xl' href={"/customer/profile"}>Thông tin tài khoản</Link>,
      key: 0,
    },
    {
      type: 'divider',
    },
    {
      label: <Link className='p-5 text-xl' href={"/customer/order/history"}>Đơn hàng của tôi</Link>,
      key: 1,
    },
    {
      type: 'divider',
    },
    {
      label: <span className='p-5 text-xl' >Đăng xuất</span>,
      key: 2,
    }
  ]


  // kiểm tra xem user đã đăng nhập hay chưa
  const { userInfo } = useAppSelector(state => state.userInfoSlice);
  
  // xây dựng hàm để render có điều kiện 
  const renderHeaderUserInfo = () => {

    if (userInfo !== null) {
      
      // trường hợp user đã đăng nhập thành công 
      return (
        <>
          <div className='logged__in__user flex flex-row gap-x-2 items-center text-2xl text-gray-500 bg-transparent'>
            <ConfigProvider
              theme={{
                components : {
                  Dropdown : {
                    paddingBlock: '20px 10px'
                  }
                }
              }}
            >
              <Dropdown
                trigger={['click', 'hover']}
                menu={{ items }}
                className='logged__in__wrapper cursor-pointer'
              >
                <Space className='hover:border hover:border-gray-400 hover:bg-gray-200 p-3 rounded transition-all'>
                  <SmileOutlined style={{fontSize: '24px'}} />
                  <span>Tài khoản</span>
                </Space>
              </Dropdown>
            </ConfigProvider>
          </div>
        </>
      )
    } else {

      // trường hợp user chưa đăng nhập 
      return (
        <>
          <Link href={'/auth/login'} className='login__button border p-2.5 border-gray-400 rounded-lg bg-[#53D260]'>
            <span className='login__title text-white transition-all'>Đăng Nhập</span>
            <UserOutlined
              style={{
                fontSize: '20px',
                fontWeight: 'bold',
                margin: '5px',
              }}
              className='login__icon'
            />
          </Link>
        </>
      )
    }

  };
  
  return (
    <div className="header__user__info w-full">
      {renderHeaderUserInfo()}
    </div>
  )
}
