'use client'
import { News } from '@/types/news';
import React from 'react';
import { ImageCustom } from '../ImageCustom/ImageCustom';
import Slider from 'react-slick';
type Props = {
  news: News[]
}

export function BannerCustom({ news }: Props) {
  const renderNewsBanner = () => {
    return news?.map((banner, index) => {
      return (
        <div key={index} className='banner__wrapper'>
          <ImageCustom width={500} height={500} isResized={true} link={banner.image} alt={banner.title}/>
        </div>
      )
    });
  };

  const settings = {
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: true,
  }

  return (
    <Slider {...settings}>
      {renderNewsBanner()}
    </Slider>
  )
}
