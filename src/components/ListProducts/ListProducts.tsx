'use client'
import { Product } from '@/types/products'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { ProductItem } from '../ProductItem/ProductItem'
import { Spin } from 'antd'
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons'

type Props = {
  allProducts: Product[]
}

export function ListProducts({ allProducts }: Props) {

  // các state cần chuẩn bị để làm tính năng cuộn sản phẩm 
  const [hasMore, setHasMore] = React.useState(true)
  const [itemsPerPage, setItemsPerPage] = React.useState(8)
  const [page, setPage] = React.useState(1) 
  const [displayedProduct, setDisplayedProduct] = React.useState<Product[]>(
    allProducts.slice(0, itemsPerPage),
  )
  const isCollapsed = React.useRef(false);



  // xây dựng hàm load các sản phẩm khi người dùng cuộn chuột xuống
  const loadMoreProducts = () => {

      const nextPage = page + 1
      const start = nextPage * itemsPerPage
      const end = start + itemsPerPage
      const moreProducts = allProducts.slice(start, end)
  
      // sau 1s -> load thên sản phẩm 
      setTimeout(() => {
        setDisplayedProduct((prevProducts) => [...prevProducts, ...moreProducts]);
        setPage(nextPage);
  
        if (end >= allProducts.length) {
          setHasMore(false);
        }
      }, 1000); // Thêm độ trễ 1 giây (1000ms)
      
    
  }

  const showLessProduct = () => {

    setHasMore(true);
    setPage(1);
    setDisplayedProduct(allProducts.slice(0, 4));
    // Scroll to the top of the product list
    isCollapsed.current = true;
    document.getElementById('product-list')?.scrollIntoView({ behavior: 'smooth' });

  }

  React.useEffect(() => {

    const handleScroll = () => {
      if (isCollapsed.current) {
        isCollapsed.current = false;
        console.log("Scrolled within product list, isCollapsed: ", isCollapsed.current);
      }
    };

    // Thêm trình nghe sự kiện cuộn cho phần tử product-list
    window.addEventListener('scroll', handleScroll);

    // Loại bỏ trình nghe sự kiện khi component bị unmount
    return () => {     
        window.removeEventListener('scroll', handleScroll);
    };
  }, []);


  const showLessDiv = () => {
    return (
      <div onClick={showLessProduct} className='w-full flex flex-row justify-center items-center mt-5 cursor-pointer'>
        <span className='border text-blue-500 border-gray-100 rounded p-3 bg-transparent hover:bg-white hover:shadow-lg transition-all'>
          Thu gọn
          <ArrowUpOutlined/> 
        </span>
      </div>
    )
  }

  const renderSpiner = () => {
    if(!isCollapsed.current){
      
      return (
        <div className='w-full flex flex-row justify-center items-center mt-5 cursor-pointer'>
          <Spin/>
        </div>
      )

    }
  }

  return (
    <InfiniteScroll
      dataLength={displayedProduct.length}
      next={loadMoreProducts}
      hasMore={hasMore}
      loader={renderSpiner()}
      endMessage={showLessDiv()}
      style={{scrollbarWidth: "none"}}
    >
      <div className="all__products__list grid grid-cols-4 gap-5">
        {displayedProduct.map((product, index) => (
          <ProductItem key={index} product={product} />
        ))}
      </div>
    </InfiniteScroll>
  )
}
