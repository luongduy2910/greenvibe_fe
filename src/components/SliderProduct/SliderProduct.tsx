'use client'
import { Product } from '@/types/products'
import React from 'react'
import Slider from 'react-slick'
import { ProductItem } from '../ProductItem/ProductItem'

type Props = {
  randomProducts: Product[]
}

export function RandomProduct({ randomProducts }: Props) {
  const settings = {
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    arrows: true,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 3000,
    padding: 30
  }
  const renderRandomProducts = () => {
    return randomProducts?.map((product, index) => {
      return (
        <div key={index} className="product__item__wrapper">
          <ProductItem product={product} />
        </div>
      )
    })
  }
  return <Slider {...settings}>
    {renderRandomProducts()}
  </Slider>
}
