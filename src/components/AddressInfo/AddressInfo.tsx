"use client"
import { useAppSelector } from '@/redux/hooks'
import { useRouter } from 'next/navigation'
import React from 'react'

type Props = {
    isIntendCart: boolean
    isCartDrawer: boolean
}

export function AddressInfo({ isIntendCart, isCartDrawer } : Props) {

    const { listAddress, chooseAddress } = useAppSelector(state => state.addressSlice);

    const defaultAddress = listAddress.filter(item => item.id === chooseAddress)!;

    const router = useRouter();

    const handleMoveAddressShippingPage = () => {
      if (isCartDrawer) {
        router.push("/checkout/shipping?isIntendCart=1")
      } else {
        router.push("/checkout/shipping")
      }
    }

  return (
   <div className={`cart__address bg-white shadow-2xl ${isCartDrawer ? 'p-10' : 'p-5'} rounded space-y-5`}>
        <div className="address__title flex flex-row justify-between items-center">
          <span className="text-xl text-gray-400">Giao tới</span>
          <span className='text-lg text-blue-500 cursor-pointer' onClick={handleMoveAddressShippingPage}>Thay đổi</span>
        </div>
        <div className="user__info text-lg font-bold flex flex-row gap-x-5">
          <span className="username block border-r-2 border-gray-500 pr-2">
            {defaultAddress[0]?.recipient_name}
          </span>
          <span className="phonenumber">{defaultAddress[0]?.recipient_phone}</span>
        </div>
        <div className="user__address__default">
          <span className="text-gray-400 text-lg">
            {defaultAddress[0]?.address}
          </span>
        </div>
    </div>
  )
}
