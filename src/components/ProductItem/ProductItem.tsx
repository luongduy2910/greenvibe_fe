'use client'
import { Product } from '@/types/products'
import React from 'react'
import { ImageCustom } from '../ImageCustom/ImageCustom'
import { convertToVND } from '@/utils'
import { FullscreenOutlined, ShoppingCartOutlined } from '@ant-design/icons'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { addToCart } from '@/redux/cartSlice'
import { Tag } from 'antd'
import { AddToCart } from '@/types/cart'

type Props = {
  product: Product
}
export function ProductItem({ product }: Props) {
  const dispatch = useAppDispatch()
  const { userInfo } = useAppSelector(state => state.userInfoSlice);

  const renderCategoryTags = () => {
    return product.category.map((tag, index) => {
      return (
        <Tag color='success' key={index}>#{tag}</Tag>
      ) 
      
    })
  };

  const handleAddToCart = async (productId: number) => {
    const req : AddToCart = {
      username: userInfo?.username!,
      id: productId,
    }

    await dispatch(addToCart(req));
    
  };

  return (
    <div className="product__item relative w-full bg-white shadow border border-gray-100 rounded cursor-pointer overflow-hidden">
      <div className="product__img h-[300px]">
        <ImageCustom
          width={400}
          height={700}
          link={product.image}
          alt={product.name}
          isResized={true}
        />
        <div className='bg__layout__prod bg-black opacity-50 w-full h-full absolute z-[5] top-0 left-0'/>
      </div>
      <div className="product__info p-2 h-[150px] flex flex-col justify-between">
        <h2 className="text-lg font-bold text-gray-700">{product.name}</h2>
        <p className="font-bold text-2xl my-2 text-red-500">{convertToVND(product.price)}</p>
        <div className='product__category flex flex-row gap-x-2'>
          {renderCategoryTags()}
        </div>
        {product.status == 1 ? (
          <div className='cart__function absolute z-[5] top-[30%] left-[20%] w-[60%]'>
            <div className="flex flex-row justify-between items-center w-full">
              <span
                className="p-3 inline-block bg-red-500 transition-all hover:bg-red-400 shadow-md rounded-xl cursor-pointer text-white hover:animate-pulse"
                onClick={() => handleAddToCart(product.id)}
              >
                <ShoppingCartOutlined style={{ color: 'white', fontSize: '35px' }} />
              </span>
              <span className="p-3 inline-block bg-blue-500 transition-all hover:bg-blue-400 shadow-md cursor-pointer text-white rounded-xl hover:animate-pulse">
                  <FullscreenOutlined style={{ color: 'white', fontSize: '35px' }} />
              </span>        
            </div>
        </div>
        ) : (
          <div className='cart__function absolute z-[5] top-[30%] left-[30%] w-[60%]'>
            <div className='flex flex-row justify-between items-center w-full'>
              <span className='text-white text-2xl font-bold'>Hết hàng</span>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}
