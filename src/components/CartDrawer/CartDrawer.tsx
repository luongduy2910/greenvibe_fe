'use client'
import React from 'react'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { Drawer } from 'antd'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { CartTable } from './CartTable/CartTable'
import { CartInfo } from './CartInfo/CartInfo'
import { setCartDrawer } from '@/redux/cartSlice'

export function CartDrawer() {

  // gọi cartDetail 
  const {cartDetails, isLoading, isOpenCartDrawer}  = useAppSelector((state) => state.cartSlice);

  console.log("cartDetails line 16", cartDetails);

  const dispatch = useAppDispatch();


  // function để mở cart drawer
  const showDrawer = () => {
    setTimeout(() => dispatch(setCartDrawer(true)) , 150); 
  }

  // function để đóng cart drawer
  const onClose = () => {
    setTimeout(() => dispatch(setCartDrawer(false)), 200);
  };
  

  return (
    <div className="cart__drawer">

      {/* Khi click vào thẻ div chứa cart icon -> bật cart drawer */}
      <div
        className="p-2 bg-[#49FF9D] border relative border-gray-50 rounded-full w-16 h-16 flex flex-row justify-center items-center cursor-pointer opacity-100 hover:opacity-70 transition-all animate-bounce"
        onClick={showDrawer}
      >
        <span className='flex flex-row justify-center items-center w-7 h-7 border absolute top-0 right-0 rounded-full bg-red-500 text-white'>
          <span className='font-bold'>
            {cartDetails?.items.length === 0 ? "0" : cartDetails?.items.length}
          </span> 
        </span>
        <ShoppingCartOutlined
          style={{ fontSize: '35px', color: 'white', fontWeight: 'bold' }}
        />

        <Drawer
          title={
            <div className="text-center text-2xl font-bold text-gray-400]">
              Giỏ hàng của {cartDetails?.username}
            </div>
          }
          onClose={onClose}
          open={isOpenCartDrawer}
          size='large'
          loading={isLoading}
        >
          <div className='cart__show space-y-5'>
            <div className='cart__table'>
              {/* Hiển thị các sản phẩm user đã thêm vào giỏ hàng  */}
              <CartTable productItems={cartDetails?.items}/>
            </div>
            <div className='cart__info__wrapper w-full'>
              <CartInfo/>
            </div>
          </div>
        </Drawer>
      </div>
    </div>
  )
}
