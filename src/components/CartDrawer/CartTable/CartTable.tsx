'use client'
import { ImageCustom } from '@/components/ImageCustom/ImageCustom'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { CartItems } from '@/types/cart'
import { DeleteOutlined } from '@ant-design/icons'
import { Table, TableProps } from 'antd'
import React from 'react'
import { deleteItems, updateChoosedItems } from '@/redux/cartSlice'
import { convertToVND } from '@/utils'

type Props = {
  productItems: CartItems[] | undefined
}

type DataType = {
  key: React.Key
  product_name: string
  price: string
  image: string
  delete: any
}

export function CartTable({ productItems }: Props) {
  const dispatch = useAppDispatch()

  // lấy các product đã được user bấm chọn mua 
  const { selectedKeys } = useAppSelector((state) => state.cartSlice)

  // tạo các columns có trong bảng product 
  const columns: TableProps<DataType>['columns'] = [
    {
      title: 'Product Name',
      dataIndex: 'product_name',
      key: 'product_name',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },

    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      render: (link: string) => (
        <ImageCustom width={50} height={50} link={link} alt="product" />
      ),
    },

    {
      title: 'Delete',
      dataIndex: 'delete',
      key: 'delete',
    },
  ]

  // dựa trên product items -> render các rows product 
  const productRows: DataType[] | undefined = productItems?.map(
    (item, index) => {
      return {
        key: item.id_products_user,
        product_name: item.product_name,
        price: convertToVND(item.price),
        image: item.image,
        delete: (
          <DeleteOutlined
            style={{ fontSize: '20px', color: 'red' }}
            className="cursor-pointer"
            onClick={() => dispatch(deleteItems([item.id_products_user]))}
          />
        ),
      }
    },
  )

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: async (
      selectedRowKeys: React.Key[],
      selectedRows: DataType[],
    ) => {
      // khi người dùng chọn/bỏ chọn product trong cart
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      )

      // gửi async thunk -> cập nhật dưới be -> sau khi thành cập -> cập nhật lại redux store
      dispatch(updateChoosedItems(selectedRowKeys));
    },
  }

  return (
    <Table
      columns={columns}
      dataSource={productRows}
      rowSelection={{
        type: 'checkbox',
        onChange: rowSelection.onChange,
        selectedRowKeys: selectedKeys,
      }}
      className='shadow-2xl bg-white rounded-lg p-4'
    />
  )
}
