'use client'
import { AddressInfo } from '@/components/AddressInfo/AddressInfo'
import { setCartDrawer } from '@/redux/cartSlice'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { convertToVND } from '@/utils'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { message } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import React from 'react'

export function CartInfo() {

  // bóc dispatch để gửi action 
  const dispatch = useAppDispatch()

  // lấy giá tiền từ redux store
  const { totalPrice, selectedKeys } = useAppSelector(
    (state) => state.cartSlice,
  )

  const { listAddress, chooseAddress } = useAppSelector(state => state.addressSlice);

  const address = listAddress.filter(item => item.id === chooseAddress);

  const { userInfo } = useAppSelector(state => state.userInfoSlice);

  const defaultAddress = userInfo?.addresses.filter(item => item.id === chooseAddress )!;

  const router = useRouter();


  const checkOut = () => {
    if (selectedKeys.length === 0) {
      message.error('Vui lòng chọn mua sản phẩm trước khi thanh toán')
    } else {
      router.push("/checkout/payment");
    }
  }

  return (
    <div className="cart__info flex flex-col gap-y-5">
      <div className="cart__total__price bg-white shadow-2xl p-10 rounded space-y-5">
        <div className="border-b border-gray-400 pb-4 flex flex-row items-center justify-between">
          <span className="title text-gray-500 text-xl">Tạm tính</span>
          <span className="tempo__price text-lg font-bold">
            {convertToVND(totalPrice)}
          </span>
        </div>
        <div className="cart__result flex flex-row items-center justify-between pt-4">
          <span className="title text-gray-500 text-xl">Tổng tiền</span>
          <span className="tempo__price text-4xl font-bold text-red-500">
            {convertToVND(totalPrice)}
          </span>
        </div>
        <div className="button__checkout flex flex-row justify-center items-center">
          <span
            className="py-5 px-7 block bg-red-500 transition-all hover:bg-red-400 border border-gray-100 shadow-md rounded text-xl cursor-pointer text-white"
            onClick={() => checkOut()}
          >
            <ShoppingCartOutlined
              style={{ color: 'white', marginRight: '5px' }}
            />
            Mua hàng({selectedKeys?.length})
          </span>
        </div>
      </div>
      <AddressInfo isIntendCart={true} isCartDrawer={true}/>
    </div>
  )
}
