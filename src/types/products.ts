
export type Product = {
    
    id: number
    name: string
    price: number
    image: string
    images: string[]
    digital: boolean
    status: number
    category: string[]
}

export type ProductWithCategoryName = {
    
    categoryName: string,
    products: Product[],

}