export type Address = {
    id: number,
    address: string,
    phoneNumber: string,
    fullName: string,
    default: number,
}

export type Province = {

    province_id: string,
    province_name: string,
    province_type: string,

}

export type District = {

    district_id: string,
    district_name: string,
    district_type: string,
    province_id: string,
}

export type Ward = {

    district_id: string,
    ward_id: string,
    ward_name: string,
    ward_type: string,

}

export type AddressUser = {

    id: number,
    recipient_name: string,
    recipient_phone: string,
    province: string,
    district: string,
    ward: string,
    street: string,
    address: string,
    status: number,
    
}

export type InsertAddress = {

    recipient_name: string,
    recipient_phone: string,
    province: string,
    district: string,
    ward: string,
    street: string,
    address: string,

}

export type InsertAddressPayload = {
    reqBody : {
        
        recipient_name: string,
        recipient_phone: string,
        province: string,
        district: string,
        ward: string,
        street: string,
        address: string,
        
    }
    username: string,
}

export type UpdateAddressPayload = {
    reqBody : {
        
        recipient_name: string,
        recipient_phone: string,
        province: string,
        district: string,
        ward: string,
        street: string,
        address: string,
        
    }
    username: string,
    address_id: number,
}

export type DeleteAddressPayload = {
    
    username: string,
    address_id: number,
    
}
