export type News = {
    
    id_news: number,
    title: string,
    image: string,

}

export type NewsCustom = {
    images: string[],
    titles: string[],
}