type Suggestion = {
    id: number,
    title: string,
}