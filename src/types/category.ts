export type Category = {
    id_category: number,
    name_category: string,
    is_sub: boolean,
    children: SubCategory[],
    image: string,
}