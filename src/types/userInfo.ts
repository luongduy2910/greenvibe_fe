import { AddressUser } from "./address";

export type UserInfo = {

    id: number;
    username: string;
    fullName: string;
    email: string;
    phone: string;
    role: string;
    addresses: AddressUser[]
    
};