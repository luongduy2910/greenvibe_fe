export type CartDetails = {
    user_id: number;
    username: string;
    total_items: number,
    items: CartItems[]
}

export type CartItems = {
    id_products_user: number;
    product_id: number;
    quantity: number;
    date_added: string;
    order_id: number;
    username: string;
    product_name: string;
    price: number;
    image: string;
    digital: boolean;
    choose: boolean;
}

export type AddToCart = {
    username: string;
    id: number;
}