export type PaymentResponse = {
    payment_url: string
    order_id: Number
    Date: Number
}

export type Column = {
    title: any,
    dataIndex: any,
    key: any,
}