import { CartDetails, CartItems } from "./cart";

export type OrderReq = {
    username: string;
    type: string;
};

export type OrderDetails = {
    id_bill: number;
    time_create: string;
    type: string;
    user_id: number,
    total: number,
    username: string,
    chosen_items: CartItems[]
}

export type VNPayOrderReq = {
    amount: number;
}

export type VNPayOrderDetails = {
    
    orderId: string;
    Date: number;
    payment_url: string;

}

export type OrderDetailsCash = {

    id_bill: number,
    time_create: string,
    type: string,
    user_id: number,
    total: number,
    username: string,
    status: number,
    chosen_items: CartItems[]

}

export type OrderDetailsVNPay = {
    
    id_bill: number,
    time_create: string,
    type: string,
    user_id: number,
    total: number,
    username: string,
    chosen_items: CartItems[]
    payment_url : string
    order_id: number,
    payment_create_date: number,

}

export type VNPaymentStatus = {
    vnp_ResponseId: string;
    vnp_Command: string;
    vnp_ResponseCode: string;
    vnp_Message: string;
    vnp_TmnCode: string;
    vnp_TxnRef: string;
    vnp_Amount: string;
    vnp_OrderInfo: string;
    vnp_BankCode: string;
    vnp_PayDate: string;
    vnp_TransactionNo: string;
    vnp_TransactionType: string;
    vnp_TransactionStatus: string;
    vnp_SecureHash: string;
}