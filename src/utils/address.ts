import { Address } from "@/types/address";

export const listAddress : Address[] = [
    {
        id: 1,
        address: '62/41, Phạm Huy Thông, P7, Quận Gò Vấp, TPHCM',
        phoneNumber: "0966553068",
        fullName: 'Greenvibe_store',
        default: 1,
    },
    {
        id: 2,
        address: '107/77, Quang Trung, P10, Quận Gò Vấp, TPHCM',
        phoneNumber: "0337572169",
        fullName: 'Green_vibe',
        default: 0,
    }
];