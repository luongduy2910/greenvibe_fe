export const convertToVND = (amount: number): string => {
    if (isNaN(amount)) {
        throw new Error("Invalid number");
    }

    // Định dạng số với dấu chấm phân cách hàng nghìn
    const formattedAmount = amount?.toLocaleString('vi-VN');
    
    // Thêm ký hiệu tiền tệ
    const vndAmount = `${formattedAmount}₫`;
    
    return vndAmount;
}