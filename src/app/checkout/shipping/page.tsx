'use client'
import { HeaderShipping } from '@/components/HeaderShipping/HeaderPayment'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import React from 'react'
import { deleteAddress, insertAddress, setChooseAddress, setInsertOrUpdate, updateAddress } from '@/redux/addressSlice'
import { ShippingAddressService } from '@/services/ShippingAddressService'
import { Input, message, Select, Tooltip, Modal } from 'antd'
import { DeleteAddressPayload, District, InsertAddressPayload, Province, UpdateAddressPayload, Ward } from '@/types/address'
import { PlusCircleOutlined } from '@ant-design/icons'
import { getUserInfo } from '@/redux/userInfoSlice'
import { useRouter } from 'next/navigation'


type Props = {
  searchParams: {
    isIntendCart: string
  }
}

export default function ShippingAddressPage({ searchParams }: Props) {

  const { isIntendCart } = searchParams;
  // const { listAddress } = useAppSelector(state => state.addressSlice);
  const { userInfo } = useAppSelector(state => state.userInfoSlice);
  // const listAddress = userInfo?.addresses;
  const {listAddress, insertOrUpdate } = useAppSelector(state => state.addressSlice);
  const [province, setProvince] = React.useState<Province[]>([]);
  const [district, setDistrict] = React.useState<District[]>([]);
  const [ward, setWard] = React.useState<Ward[]>([]);
  const [ fullName, setFullName ] = React.useState<string>("");
  const [ phoneNumber, setPhoneNumber ] = React.useState<string>("");
  const [ addressinfo, setAddressinfo ] = React.useState<string>("");
  const [provinceSelected, setProvicedSelected] = React.useState<string>("");
  const [districtSelected, setDistrictSelected] = React.useState<string>("");
  const [wardSelected, setWarddSelected] = React.useState<string>("");
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleMoveHomePageOrPaymentPage = (addressId: number) => {
    dispatch(setChooseAddress(addressId));
    console.log(isIntendCart);
    if(isIntendCart){
      router.push("/");
    } else {
      router.push("/checkout/payment");
    }
  }

  const { TextArea } = Input;

  const renderAdressItems = () => {
    return listAddress?.map((address, index) => {
      return (
        <li
          className={`border-2 ${
            address?.status === 1
              ? 'border-dashed border-[#53D260]'
              : 'border-gray-400'
          } p-3 flex flex-col gap-y-3`}
          key={index}
        >
          <div className="full__name flex flex-row justify-between items-center">
            <span className="text-lg text-black font-bold">
              {address?.recipient_name}
            </span>
            {address?.status === 1 ? (
              <span className="text-lg text-[#53D260] font-semibold">
                Mặc định
              </span>
            ) : null}
          </div>
          <div className="adress__info">
            <span className="text-gray-400 text-lg">{address?.address}</span>
          </div>
          <div className="phone__number">
            <span className="text-gray-400 text-lg">
              Điện thoại: {address?.recipient_phone}
            </span>
          </div>
          <div className="address__func flex flex-row gap-2 items-center">
            <span
              className={`${
                address?.status === 1 ? 'bg-blue-500' : 'bg-gray-500'
              } text-white font-semibold border border-gray-400 rounded px-3 py-2 cursor-pointer`}

              onClick={() => handleMoveHomePageOrPaymentPage(address?.id)} 
            >
              Giao tới địa chỉ này
            </span>
            <span
              className={`bg-transparent text-black font-semibold border border-gray-400 rounded px-3 py-2 cursor-pointer`}
              onClick={() => handleUpdateAdress(address?.id)}
            >
              Sửa
            </span>
            {address?.status !== 1 ? (
              
              <span
                className={`bg-transparent text-black font-semibold border border-gray-400 rounded px-3 py-2 cursor-pointer`}
                onClick={() => handleDeleteAddress(address.id)}
              >
                Xóa
              </span>
            ) : null}
          </div>
        </li>
      )
    })
  }

  const renderProvinceOptions = province?.map(item => {
    
    return {
      label: item.province_name,
      value: item.province_id,
    }
  })

  const renderDistrictOptions = district?.map(item => {
    return {
      label: item.district_name,
      value: item.district_id,
    }
  })

  const renderWardOptions = ward?.map(item => {
    return {
      label: item.ward_name,
      value: item.ward_id,
    }
  })

  const fetchProvince = async () => {
    const province = await ShippingAddressService.getProvince();
    console.log("province", province);
    setProvince(province);
  }


  // handle các tỉnh/thành quận/huyện phường/xã 

  const handleProvinceChange = async (value : string) => {
    console.log("province_id", value);
    
    setProvicedSelected(province?.filter(item => item.province_id === value)[0].province_name);
    if(value !== provinceSelected) {
      setDistrictSelected("");
      setWarddSelected("");
    }
    const listDistrict = await ShippingAddressService.getDistrict(value);
    setDistrict(listDistrict);
  }

  const handleDistrictChange = async (value : string) => {
    console.log("district", value);
    setDistrictSelected(district.filter(item => item.district_id === value)[0].district_name);
    if(value !== districtSelected) {
      setWarddSelected("");
    }
    const listWard = await ShippingAddressService.getWard(value);
    setWard(listWard);
  }

  const handleWard = (value : string) => {
    console.log("ward", value);
    setWarddSelected(ward.filter(item => item.ward_id === value)[0].ward_name);
  }

  const resetAddressInfo = () => {
    setFullName("");
    setAddressinfo("");
    setDistrictSelected("");
    setProvicedSelected("");
    setWarddSelected("");
    setPhoneNumber("");
  };

  const handleInsertAddress = async () => {

    if(!fullName || !phoneNumber || !province || !district || !ward || !addressinfo){
      message.error("Vui lòng nhập đầy đủ thông tin địa chỉ");
    }else {

      if(insertOrUpdate === 0){


        const fullAddress = addressinfo + "," + wardSelected + "," + districtSelected + "," + provinceSelected;
          
          const payloadAddress : InsertAddressPayload = {
            
            reqBody: {
              recipient_name: fullName,
              recipient_phone: phoneNumber,
              street: addressinfo,
              province: provinceSelected,
              district: districtSelected,
              ward: wardSelected,
              address: fullAddress,
            },
            username: userInfo?.username!
          };
    
          console.log("payload", payloadAddress);
    
          const result = await dispatch(insertAddress(payloadAddress));
  
          if(insertAddress.fulfilled.match(result)){
  
            // nếu thêm địa chỉ thành công -> reset form và hiển thị thông báo tương ứng 
            resetAddressInfo();
            handleCancel();
  
          }else {
  
            // nếu thêm địa chỉ thất bại -> hiển thị thông báo 
            message.error("Thêm địa chỉ thất bại");
          }
      }else {
        const fullAddress = addressinfo + "," + wardSelected + "," + districtSelected + "," + provinceSelected;
        const payloadAddress : UpdateAddressPayload = {
          reqBody: {
            recipient_name: fullName,
            recipient_phone: phoneNumber,
            street: addressinfo,
            province: provinceSelected,
            district: districtSelected,
            ward: wardSelected,
            address: fullAddress,
          },
          username: userInfo?.username!,
          address_id: insertOrUpdate
        }

        console.log(payloadAddress);

        const result = await dispatch(updateAddress(payloadAddress));

        if(updateAddress.fulfilled.match(result)){
          resetAddressInfo();
          handleCancel();
        }

      }
    }


  };

  const handleDeleteAddress = async (addressId: number) => {

    const payload: DeleteAddressPayload = {
      username: userInfo?.username!,
      address_id: addressId
    }

    if(payload) {

      await dispatch(deleteAddress(payload));
      
    }
  }

  const showModal = () => {
    setIsOpen(true);
    dispatch(setInsertOrUpdate(0));
  };

  const handleOk = () => {
    setIsOpen(false);
  };

  const handleCancel = () => {
    setIsOpen(false);
    resetAddressInfo();
  };

  const handleUpdateAdress = (addressId: number) => {

    dispatch(setInsertOrUpdate(addressId));

    const chooosedAddress = listAddress.filter(item => item.id === addressId);

    const { recipient_name, recipient_phone, address } = chooosedAddress[0];

    const addressSplit = address.split(",");

    setFullName(recipient_name);
    setPhoneNumber(recipient_phone);
    setAddressinfo(addressSplit[0] + "," + addressSplit[1])
    setWarddSelected(addressSplit[2])
    setDistrictSelected(addressSplit[3])
    setProvicedSelected(addressSplit[4])

    setIsOpen(true);

  }

  React.useEffect(() => {
    fetchProvince();
  },[])

  return (
    <div className='flex flex-col gap-y-10 w-full min-h-screen relative'>
      <HeaderShipping />
      <ul className="p-2 mt-10 grid grid-cols-3 gap-5 w-[80%] mx-auto">
        {renderAdressItems()}
      </ul>
      <Modal
        open={isOpen}
        width={1000}
        onCancel={handleCancel}
        okButtonProps={{style: {display: "none"}}}
        cancelButtonProps={{style: {display: "none"}}}
      >
        <div className='form__add__or__update__address my-5 p-2 w-[80%] mx-auto space-y-10'>
          <h3 className='text-2xl font-semibold'>{insertOrUpdate === 0 ? "Thêm địa chỉ mới" : "Cập nhật địa chỉ"}</h3>
          <div className='form__address flex flex-col gap-y-10'>
            <div className='full__name flex flex-row'>
              <span className='fullName__label w-[40%] text-xl font-semibold'>Họ tên</span>
              <Input value={fullName} onChange={e => setFullName(e.target.value)} size='large' style={{width: '500px'}}/>
            </div>
            <div className='phone__number flex flex-row'>
              <span className='phoneNumber__label w-[40%] text-xl font-semibold'>Số điện thoại</span>
              <Input value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} size='large' style={{width: '500px'}}/>
            </div>
            <div className='flex flex-row items-center'>
              <span className='province__label w-[40%] text-xl font-semibold'>Chọn thành Tỉnh/Thành Phố</span>
              <Select
                placeholder="Vui lòng chọn tỉnh/thành"
                style={{width: '500px'}}
                options={[...renderProvinceOptions]}
                onChange={handleProvinceChange}
                value={provinceSelected}
                size='large'
                
              />
            </div>
            <div className='flex flex-row items-center'>
              <span className='district__label w-[40%] text-xl font-semibold'>Chọn Quận/Huyện</span>
              <Select
                placeholder="Vui lòng chọn quận/huyện"
                style={{width: '500px'}}
                options={[...renderDistrictOptions]}
                onChange={handleDistrictChange}
                value={districtSelected}
                size='large'

              />
            </div>
            <div className='flex flex-row items-center'>
              <span className='ward__label w-[40%] text-xl font-semibold' >Chọn Phường/Xã</span>
              <Select
                placeholder="Vui lòng chọn phường/xã"
                style={{width: '500px'}}
                options={[...renderWardOptions]}
                value={wardSelected}
                onSelect={handleWard}
                size='large'

              />
            </div>
            <div className='flex flex-row items-center'>
              <span className='ward__label w-[40%] text-xl font-semibold' >Địa chỉ</span>
              <TextArea
                placeholder="Ví dụ: 900, Quốc lộ 1"
                style={{width: '500px'}}
                size='large'
                onChange={e => setAddressinfo(e.target.value)}
                value={addressinfo}

              />
            </div>
            <div className='flex flex-row items-center gap-x-5 justify-end'>
              <span
                className='text-white font-semibold border border-gray-400 rounded px-3 py-2 cursor-pointer bg-gray-500'
                onClick={handleCancel}
              >
                Hủy bỏ
              </span>
              <span
                className='text-white font-semibold border border-gray-400 rounded px-3 py-2 cursor-pointer bg-blue-500'
                onClick={handleInsertAddress}
              >
                {insertOrUpdate === 0 ? "Giao tới địa chỉ này" : "Cập nhật"}
              </span>
            </div>  
          </div>
        </div>
      </Modal>
      <Tooltip title="Thêm địa chỉ mới">
        <div
          className="p-2 bg-[#49FF9D] border absolute right-12 top-[200px] border-gray-50 rounded-full w-16 h-16 flex flex-row justify-center items-center cursor-pointer opacity-100 hover:opacity-70"
          onClick={showModal}
        >
          <PlusCircleOutlined style={{color: 'white', fontSize: '30px'}}/>
        </div>
      </Tooltip>
    </div>
  )
}
