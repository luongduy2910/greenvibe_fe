import React from 'react'

export const dynamic = 'force-dynamic'

export default function CheckoutLayout({ children } : Readonly<{children: React.ReactNode}>) {
  return (
    <div>
        {children}
    </div>
  )
}
