import { HeaderPayment } from '@/components/HeaderPayment/HeaderPayment'
import React from 'react'

export default function CheckoutPaymentLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
      <div>
        <HeaderPayment/>
        {children}
      </div>
  ) 
  
}
