'use client'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import {
  createOrderByPaymentMethod,
} from '@/redux/orderSlice'
import { OrderReq } from '@/types/order'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { useRouter } from 'next/navigation'
import React from 'react'
import Link from 'next/link'
import { convertToVND } from '@/utils'
import { setPaymentMethod, toggleExpandItems } from '@/redux/paymentSlice'
import { ImageCustom } from '@/components/ImageCustom/ImageCustom'
import { Radio, Space } from 'antd'
import type { RadioChangeEvent } from 'antd'
import CashPng from '../../../../public/images/tien_mat.png'
import VNPayPng from '../../../../public/images/vn_pay.png'
import Image from 'next/image'
import { AddressInfo } from '@/components/AddressInfo/AddressInfo'

export default function CheckoutPaymentPage() {
  // từ cart details -> lấy products đã chọn , tổng giá -> làm trang thanh toán
  // sau khi chọn hình thức thanh toán và thanh toán thành công => tạo bill

  // bóc dispatch để gửi action
  const dispatch = useAppDispatch()

  const { expandItems, paymentMethod } = useAppSelector(
    (state) => state.paymentSlice,
  )

  // từ cart redux store -> lấy cart details và tổng giá của các sản phẩm đã chọn
  const { cartDetails, totalPrice } = useAppSelector((state) => state.cartSlice)

  const { listAddress, chooseAddress } = useAppSelector(state => state.addressSlice);

  const address = listAddress.filter(item => item.id === chooseAddress);

  const { userInfo } = useAppSelector(state => state.userInfoSlice);

  const defaultAddress = userInfo?.addresses.filter(item => item.id == chooseAddress)!;

  const chosenItems = cartDetails?.items.filter((item) => item.choose === true)

  // const { idProductUsers, productIds } = useAppSelector((state) => state.orderSlice);

  // ---- các dữ liệu để làm trang thanh toán: [ cartDetails, chosenItems, totalPrice] -------

  // sử dụng router để điều hướng trang
  const router = useRouter()

  const renderOrderItemsInfo = () => {
    return chosenItems?.map((item, index) => {
      return (
        <li
          key={index}
          className="item__info flex flex-row justify-between text-lg"
        >
          <span className="text-gray-300 ">1x</span>
          <span className="font-semibold">{item.product_name}</span>
          <span className="font-semibold">{convertToVND(item.price)}</span>
        </li>
      )
    })
  }

  const renderChosenProduct = () => {
    return chosenItems?.map((item, index) => {
      return (
        <li
          key={index}
          className="flex flex-row gap-x-5 items-center justify-between"
        >
          <ImageCustom
            width={100}
            height={100}
            alt={item.product_name}
            link={item.image}
          />
          <span className="text-gray-700 text-xl">{item.product_name}</span>
          <span className="text-gray-700 text-xl">
            {convertToVND(item.price)}
          </span>
        </li>
      )
    })
  }


  const changePaymentMethod = (e: RadioChangeEvent) => {

    console.log('radio checked', e.target.value)
    dispatch(setPaymentMethod(e.target.value))

  }

  const createBillByPaymentMethod = async () => {
    
    // chuẩn bị dữ liệu để gửi lên server tạo bill
    const req: OrderReq = {
      username: cartDetails?.username as string,
      type: String(paymentMethod),
    }

    const resultAcion = await dispatch(createOrderByPaymentMethod(req));

    if (createOrderByPaymentMethod.fulfilled.match(resultAcion)) {
      const orderId = localStorage.getItem('order_id')
      if(paymentMethod === 0){

        router.push(`/checkout/payment/success?status=completed&order_id=${orderId}`)
      }else {

        router.push(`/checkout/payment/success?status=pending&order_id=${orderId}`)

      }
    }
  }

  return (
    <section className="payment__content bg-[#F5F5FB] w-full min-h-screen">
      <div className="payment__content__wrapper w-[80%] mx-auto p-5 flex flex-row justify-between">
        <div className="payment__left w-[70%] flex flex-col gap-y-5">
          <div className="bg-white p-10 rounded border-gray-200">
            <h2 className="font-bold text-2xl">Chọn hình thức giao hàng</h2>
            <ul className="display__product flex flex-col gap-y-5 mt-10 w-[90%] mx-auto">
              {renderChosenProduct()}
            </ul>
          </div>
          <div className="bg-white p-10 rounded border-gray-200">
            <h2 className="font-bold text-2xl">Chọn hình thức thanh toán</h2>
            <div className="payment__method mt-10">
              <Radio.Group
                size="large"
                onChange={changePaymentMethod}
                value={paymentMethod}
              >
                <Space className="space-y-5" direction="vertical">
                  <Radio value={0}>
                    <div className="flex flex-row items-center gap-5">
                      <Image
                        src={CashPng}
                        height={50}
                        width={50}
                        alt="tien_mat"
                      />
                      <span className="text-xl text-gray-400 font-semibold">
                        Thanh toán tiền mặt
                      </span>
                    </div>
                  </Radio>
                  <Radio className="flex flex-row" value={1}>
                    <div className="flex flex-row items-center gap-5">
                      <Image
                        src={VNPayPng}
                        height={50}
                        width={50}
                        alt="tien_mat"
                      />
                      <span className="text-xl text-gray-400 font-semibold">
                        VN Pay
                      </span>
                    </div>
                  </Radio>
                </Space>
              </Radio.Group>
            </div>
          </div>
        </div>
        <div className="payment__right w-[25%] flex flex-col gap-y-5">
          <AddressInfo isIntendCart={false} isCartDrawer={false}/>
          <div className="order__info bg-white shadow-2xl rounded space-y-5">
            <div className="order__info__top p-3 border-b border-gray-200 flex flex-row justify-between items-center">
              <div className="top__left flex flex-col gap-y-2">
                <h2 className="text-xl font-semibold">Đơn hàng</h2>
                <div>
                  <span className="text-lg text-gray-400">
                    {chosenItems?.length === 0 ? "0" : chosenItems?.length} sản phẩm.
                  </span>
                  <span
                    className="text-lg text-blue-500 cursor-pointer"
                    onClick={() => dispatch(toggleExpandItems(!expandItems))}
                  >
                    {expandItems ? 'Thu gọn' : 'Xem thông tin'}
                  </span>
                </div>
              </div>
              <div className="top__right">
                <span className="text-blue-500 cursor-pointer" onClick={() => router.push("/")}>
                  Thay đổi
                </span>
              </div>
            </div>
            {expandItems && (
              <ul className="order__items__info p-3 border-b border-gray-200 flex flex-col gap-y-2">
                {renderOrderItemsInfo()}
              </ul>
            )}
            <div className="order__info__mid p-3 border-b border-gray-200 flex flex-col gap-y-2">
              <div className="total__price flex flex-row items-center justify-between">
                <span className="text-gray-400 text-lg">Tạm tính</span>
                <span className="text-lg font-semibold">
                  {convertToVND(totalPrice)}
                </span>
              </div>

              <div className="shipping__price flex flex-row items-center justify-between">
                <span className="text-gray-400 text-lg">Phí vận chuyển</span>
                <span className="text-lg font-semibold">
                  {convertToVND(totalPrice)}
                </span>
              </div>
            </div>
            <div className="order__bottom p-3 flex flex-col gap-y-5">
              <div className="final__price flex flex-row items-center justify-between">
                <span className="font-semibold text-lg">Tổng tiền</span>
                <span className="text-2xl text-red-500">
                  {convertToVND(totalPrice)}
                </span>
              </div>
              <div className="order__button">
                <span
                  onClick={() => createBillByPaymentMethod()}
                  className="py-5 px-7 block bg-red-500 transition-all hover:bg-red-400 border border-gray-100 shadow-md rounded text-xl cursor-pointer text-white text-center"
                >
                  <ShoppingCartOutlined
                    style={{ color: 'white', marginRight: '5px' }}
                  />
                  Đặt hàng
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
