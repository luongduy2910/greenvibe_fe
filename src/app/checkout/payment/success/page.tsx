'use client'
import { deleteItems, setDefaultAfterOrderSucess, updateCartAfterSuccessOrder } from '@/redux/cartSlice'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { CartService } from '@/services/CartService'
import { ProductService } from '@/services/ProductService'
import { convertToVND } from '@/utils'
import React from 'react'
import { ImageCustom } from '@/components/ImageCustom/ImageCustom'
import { OrderService } from '@/services/OrderService'
import { useRouter, useSearchParams, usePathname } from 'next/navigation'
import { setOrderDetails } from '@/redux/orderSlice'
import Image from 'next/image'
import WaitingIcon from "../../../../../public/images/logo_wating-14.png"
import SuccessIcon from "../../../../../public/images/logo_success-15.png"

type Props = {
  searchParams: {
    order_id: string,
    status: string
  }
}

export default function ConfirmationOrderPage({ searchParams }: Props) {
  const { order_id, status } = searchParams
  console.log('orderId', order_id)
  
  const [paymentStatus, setPaymentStatus] = React.useState(status);

  const dispatch = useAppDispatch()
  const router = useRouter();
  const paramsearching = useSearchParams();
  const pathname = usePathname();

  // Bóc các state từ order redux
  const { productIds, orderDetails, idProductUsers } = useAppSelector(
    (state) => state.orderSlice,
  )

  const hasRun = React.useRef(false)

  // Hàm chạy ngầm khi user thanh toán thành công
  const orderCreateSuccess = async () => {
    if (
      order_id === localStorage.getItem('order_id') &&
      orderDetails?.type !== null
    ) {

      // Xóa sản phẩm đã mua ra khỏi giỏ hàng
      const cartDetails = await CartService.deleteItems(idProductUsers)

      // set các biến về mặc định sau khi thanh toán tiền mặt thành công 
      dispatch(setDefaultAfterOrderSucess());

      // cập nhật lại giỏ hàng 
      dispatch(updateCartAfterSuccessOrder(cartDetails));

      // Cập nhật lại trạng thái sản phẩm
      await ProductService.changeStatusProductsto0(productIds)
    }
  }

  // Hàm kiểm tra trạng thái payment của VNPay 
  const checkVNPay = async () => {
    if (paymentStatus === 'pending') {
      const vnpStatus = await OrderService.checkVNPayStatus(Number(order_id));
      console.log(vnpStatus);
  
      if (vnpStatus !== null) {
        if (vnpStatus.vnp_TransactionStatus === "00") {
          // Cập nhật lại trạng thái để hiển thị thông báo đặt hàng thành công 
          setPaymentStatus('completed');
          
          // Thay đổi URL chỉ với tham số status
          const params = new URLSearchParams(paramsearching.toString());
          params.set('status', 'completed');
          router.replace(`${pathname}?${params.toString()}`);
  
          // Gọi API để thay đổi trạng thái đơn hàng thành công khi thanh toán VNPay 
          await OrderService.changeOrderStatus(Number(order_id));
  
          // Gọi hàm chạy ngầm khi thanh toán VNPay thành công
          await orderCreateSuccess();
        }
      }
    }
  }

  const renderChosenProduct = () => {
    return orderDetails?.chosen_items.map((item, index) => {
      return (
        <li
          key={index}
          className="flex flex-row gap-x-5 items-center justify-between"
        >
          <ImageCustom
            width={100}
            height={100}
            alt={item.product_name}
            link={item.image}
          />
          <span className="text-gray-700 text-xl">{item.product_name}</span>
          <span className="text-gray-700 text-xl">
            {convertToVND(item.price)}
          </span>
        </li>
      )
    })
  }

  React.useEffect(() => {
    if (!hasRun.current && productIds.length > 0 && idProductUsers.length > 0 && orderDetails?.type === "0") {
      
      // thanh toán bằng tiền mặt 
      hasRun.current = true
      orderCreateSuccess()
    }

    if (paymentStatus === 'pending') {
      const interval = setInterval(checkVNPay, 20000);
      return () => clearInterval(interval);
    }
  }, [paymentStatus])

  console.log('chosenItems in success', productIds)
  return (
    <div className='payment__success bg-[#F5F5FB] w-full'>
      <div className='payment__success__content w-[80%] px-2 py-10 flex flex-row justify-between mx-auto min-h-screen'>
        {paymentStatus === 'completed' ? (
          <>
            <div className='flex flex-col w-[60%] rounded'>
              <div className='bg-[#53D260] text-center p-5'>
                <h2 className='text-white text-5xl font-bold'>Đặt hàng thành công</h2>
              </div>
              <div className='flex flex-row gap-x-2 bg-white shadow rounded p-5'>
                <div className='w-[30%] icon__success'>
                  <Image width={300} height={300} src={SuccessIcon} alt='success_logo'/>
                </div>
                <div className='payment__noti flex flex-col gap-y-5 w-full'>
                  <div className='payment__method flex flex-row items-center justify-between border-b border-gray-400 pb-6'>
                    <h2 className='text-xl font-semibold text-gray-500'>Phương thức thanh toán</h2>
                    <span className='text-black text-2xl font-semibold'>{orderDetails?.type === "0" ? "Thanh toán tiền mặt" : "Thanh toán qua VN Pay"}</span>
                  </div>
                  <div className='payment__total flex flex-row items-center justify-between pb-6'>
                    <h2 className='text-xl font-semibold text-gray-500'>Tổng cộng</h2>
                    <span className='text-black text-2xl font-semibold'>{convertToVND(orderDetails?.total!)}</span>
                  </div>
                  <div className='back__to__home mx-auto flex flex-row justify-center items-center w-full'>
                    <div onClick={() => router.push("/")} className='border border-[#53D260] rounded bg-transparent w-[60%] text-[#53D260] p-5 text-center text-xl hover:bg-[#53D260] hover:text-white hover:border-white transition-all cursor-pointer'>
                      Quay về trang chủ
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div className='order__details w-[35%] bg-white shadow rounded p-5 flex flex-col gap-y-5 h-fit'>
              <div className='order__code flex flex-row justify-between items-center'>
                <h2 className='font-bold text-xl'>Mã đơn hàng: {orderDetails?.id_bill}</h2>
                <span className='text-blue-500 cursor-pointer'>Xem đơn hàng</span>
              </div>
              <ul className='mt-10'>
                {renderChosenProduct()}
              </ul>
            </div>
          </>
        ) : (
          <>
            <div className='w-[50%] rounded'>
              <Image width={700} height={700} src={WaitingIcon} alt='waiting_logo'/>
            </div>
            <div className='w-[35%] flex flex-row justify-center items-center'>
              <div className='flex flex-col gap-y-10'>
                <h2 className='text-4xl font-bold text-gray-700'>Vui lòng thanh toán đơn hàng</h2>
                <span onClick={() => router.push('/checkout/payment')} className='w-full border border-[#53D260] rounded bg-transparent text-[#53D260] p-5 text-center text-xl hover:bg-[#53D260] hover:text-white hover:border-white transition-all cursor-pointer'>
                  Quay về trang thanh toán
                </span>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
