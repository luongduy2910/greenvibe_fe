import { HeaderCustom } from '@/components/HeaderCustom/HeaderCustom'
import React from 'react'

type Props = {
  searchParams: {
    q: string
  }
}

export default function SearchPage({ searchParams }: Props) {


  return (
    <section id='search__result'>
      <HeaderCustom/>
    </section>
  ) 
}
