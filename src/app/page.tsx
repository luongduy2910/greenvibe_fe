import React from "react";
import { CategorySideBar } from "@/components/CategorySideBar/CategorySideBar";
import { NewsService } from '@/services/NewsService'
import { ProductService } from '@/services/ProductService'
import { News } from '@/types/news'
import { Product, ProductWithCategoryName } from '@/types/products'
import { BannerCustom } from "@/components/BannerCustom/BannerCustom";
import Image from "next/image";
import Promotion from "../../public/images/promotion.png"
import { RandomProduct } from "@/components/SliderProduct/SliderProduct";
import { BarsOutlined } from "@ant-design/icons";
import { ListProducts } from "@/components/ListProducts/ListProducts";
import HottingIcon from "../../public/images/hotting.png";
import SalesIcon from "../../public/images/sales-png-5.jpg";
import Recommended from "../../public/images/recmmended.jpg";
import { SubCategoryService } from "@/services/SubCategoryService";
import { Tag } from "antd";
import { HeaderCustom } from "@/components/HeaderCustom/HeaderCustom";
import { CartDrawer } from "@/components/CartDrawer/CartDrawer";
import { LoadingLogo } from "@/components/LoadingLogo/LoadingLogo";
import { BgOverLayout } from "@/components/BgOverLayout/BgOverLayout";


// thời gian tái tạo lại trang
export const revalidate = 10

//NOTE - Liệt kê các api cần phải gọi để xây dựng trang home page 


async function getNews(): Promise<News[]> {
  const banners = await NewsService.getBanner();
  return banners;
}

async function getRandomProducts(): Promise<Product[]> {
  const products = await ProductService.getRandomProducts();
  return products;
}

async function getFlashSaleProducts() : Promise<Product[]> {
  const flashSaleProduct = await ProductService.getFlashSaleProducts();
  return flashSaleProduct;
}

async function getRandomProductByCategory(): Promise<ProductWithCategoryName[]> {
  try {
    // Lấy tất cả các subCategories
    const subCategories = await SubCategoryService.getAll();

    // Tạo mảng các Promise<ProductWithCategoryName>
    const promises = subCategories.slice(0, 2).map(async (item1) => {
      // Lấy danh sách sản phẩm cho từng subCategory
      const products = await ProductService.getProductsByCategory(item1.name_category);
      
      // Trả về object ProductWithCategoryName
      return {
        categoryName: item1.name_category,
        products: products,
      };
    });

    // Đợi tất cả các Promise hoàn thành và trả về kết quả
    return Promise.all(promises);
  } catch (error) {
    console.error('Error in getRandomProductByCategory:', error);
    // Xử lý lỗi nếu cần thiết và trả về một giá trị mặc định hoặc rỗng
    return [];
  }
}


async function getAllProducts() : Promise<Product[]> {
  const products = await ProductService.getAllProducts();
  return products;
}

//NOTE - Kết thúc ------------------------



export default async function HomePage() {

  const banners = await getNews();
  const randomProducts = await getRandomProducts();
  const flashSaleProducts = await getFlashSaleProducts();
  const allProducts = await getAllProducts();
  const productsByCategory = await getRandomProductByCategory();

  const productCustom = [...allProducts, ...flashSaleProducts, ...randomProducts, ...allProducts, ...flashSaleProducts]

  console.log('banner', banners)
  console.log('products', randomProducts)
  console.log("flashSaleProduct", flashSaleProducts);
  console.log("all products", allProducts);



  return (
    <section id="home__page">
      {/* Header trong trang home page  */}
      <HeaderCustom/>

      {/* Button cart hiển thị tất cả các trang -> ngoại trừ nhóm /admin  */}
      <div className='fixed top-[220px] right-10 z-10'>
        <CartDrawer/>
      </div>

      {/* Hiệu ứng loading screen mỗi khi user vào trang chủ  */}
      <LoadingLogo/>

      {/* Trong main homepage gồm 2 components chính  */}
      <main className="w-4/5 mx-auto p-2 my-5 flex flex-row justify-between">

        {/* Component1: Category sidebar -> cố định, có thể trượt xuống  */}
        <div className="section__sidebar w-[18%] flex flex-col gap-y-5 bg-white shadow-2xl rounded pb-[200px]">
          <CategorySideBar/>
          <CategorySideBar/>
        </div>

        {/* Component2: Nội dung trang web -> bao gồm banner , các loại products  */}
        <div className="section__main w-[80%] flex flex-col gap-y-20">

          {/* Component2.1: Banner  */}
          <div className="section__banner bg-white p-5 shadow-xl rounded-lg border border-gray-100">
            <BannerCustom news={banners}/>
          </div>

          {/* Component2.1: Banner promotion  */}
          <div className="section__promotion ">
            <Image width={1200} height={120} src={Promotion} alt="promotion_png" className="w-full h-full"/>
          </div>

          {/* Component2.2: Các product ngẫu nhiên  */}
          <div id='product-list' className="section__product__random bg-white p-5 shadow-xl rounded-lg border border-gray-100">
            <h2 className="text-2xl text-gray-700 font-semibold my-5 flex flex-row gap-2 items-center">
              <Image width={100} height={100} src={HottingIcon} alt="hot_img" />
              Sản phẩm mới nhất
            </h2>
            <RandomProduct randomProducts={randomProducts}/>
          </div>

          {/* Component2.3: Các product đang sale */}
          <div className="section__product__sales bg-white p-5 shadow-xl rounded-lg border border-gray-100">
            <h2 className="text-2xl text-gray-700 font-semibold my-5 flex flex-row gap-2 items-center">
              <Image width={100} height={100} src={SalesIcon} alt="sales_img"/>
              Sản phẩm đang sale
            </h2>
            <RandomProduct randomProducts={flashSaleProducts}/>
          </div>

          {/* Component2.4: Các product của 1 category  */}
          {productsByCategory.map((item , index) => {
            return (
              <div key={index} className="section__product__by__category bg-white p-5 shadow-xl rounded-lg border border-gray-100">
                <h2 className="text-2xl text-gray-700 font-semibold my-5 flex flex-row gap-2 items-center">
                  <BarsOutlined style={{fontSize: '20px'}}/>
                  Sản phẩm về
                  <Tag style={{ fontSize: '20px', padding: '10px', color: 'red'}}>
                    {item.categoryName}
                  </Tag>
                </h2>
                <RandomProduct randomProducts={item.products}/>
              </div>
            )
          })}
          {/* Component2.5: Toàn bộ product  */}
          <div className="section__product__all bg-white p-5 shadow-xl rounded-lg border border-gray-100">
            <h2 className="text-2xl text-gray-700 font-semibold my-5 flex flex-row gap-2 items-center">
            <Image width={100} height={100} src={Recommended} alt="sales_img"/>
              Tất cả sản phẩm
            </h2>
            <ListProducts allProducts={productCustom}/>
          </div>
          
        </div>
      </main>
    </section>
  );
}

