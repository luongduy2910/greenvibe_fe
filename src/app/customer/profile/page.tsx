'use client'
import React from 'react'
import { ConfigProvider, Dropdown, Input, Space, Upload } from 'antd'
import type { GetProp, MenuProps, UploadFile, UploadProps } from 'antd'
import ImgCrop from 'antd-img-crop'
import { message } from 'antd'
import {
  LoadingOutlined,
  PlusOutlined,
  UserOutlined,
  EditOutlined,
  AimOutlined,
  FolderViewOutlined,
  DeleteOutlined,
} from '@ant-design/icons'
import { useAppSelector } from '@/redux/hooks'

export default function ProfileUserPage() {
  const { userInfo } = useAppSelector(state => state.userInfoSlice);
  
  const [fullName, setfullName] = React.useState<string>(userInfo?.fullName!);
  const [phoneNumber, setPhoneNumber] = React.useState<string>(userInfo?.phone!);

  type FileType = Parameters<GetProp<UploadProps, 'beforeUpload'>>[0]
  const getBase64 = (img: FileType, callback: (url: string) => void) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result as string))
    reader.readAsDataURL(img)
  }
  const beforeUpload = (file: FileType) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'

    // kiểm tra định dạng của file có phải là .jpg hay .png
    if (!isJpgOrPng) {
      // nếu không phải -> báo lỗi
      message.error('Vui lòng tải file ảnh .png hoặc .jpeg')
    }

    // kiểm tra dung lượng của file có vượt quá 2MB hay không
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      // nếu vượt quá -> báo lỗi
      message.error('Dung lượng ảnh tối đa là 2MB')
    }
    return isJpgOrPng && isLt2M
  }
  const [loading, setLoading] = React.useState(false)
  const [imageUrl, setImageUrl] = React.useState<string>("")

  const handleChange: UploadProps['onChange'] = (info) => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return
    }
    if (info.file.status === 'done') {
      // gọi API cập nhật ảnh đại diện sau khi đã kiểm tra các điều kiện cần và đủ để upload avatar

      // Get this url from response in real world.
      getBase64(info.file.originFileObj as FileType, (url) => {
        console.log(info.file.originFileObj)
        setLoading(false)
        setImageUrl(url)
      })
    }
  }

  const uploadButton = (
    <button style={{ border: 0, background: 'none' }} type="button">
      {loading ? (
        <LoadingOutlined />
      ) : (
        <UserOutlined style={{ fontSize: '30px' }} />
      )}
      <div style={{ marginTop: 8 }}>Avatar</div>
    </button>
  )

  const alreadyUploadedOptions: MenuProps['items'] = [
    {
      key: 0,
      label: <span className="text-lg text-gray-700">Xem ảnh đại diện</span>,
      icon: <FolderViewOutlined style={{ fontSize: '18px' }} />,
    },
    {
      type: 'divider',
    },
    {
      key: 1,
      label: (
        <ImgCrop
          rotationSlider={true}
          quality={1}
          modalTitle='Chỉnh sửa ảnh'
          modalWidth={1000}
        >
          <Upload
            beforeUpload={beforeUpload}
            onChange={handleChange}
            className="text-lg text-gray-700"
            showUploadList={false}
          >
            <span className='text-lg text-gray-700'>Cập nhật ảnh đại diện</span>
          </Upload>
        </ImgCrop>
      ),
      icon: <AimOutlined style={{ fontSize: '18px' }} />,
    },
    {
      type: 'divider',
    },
    {
      key: 2,
      label: (
        <span className="text-lg text-gray-700">Xóa ảnh đại diện hiện tại</span>
      ),
      icon: <DeleteOutlined style={{ fontSize: '18px' }} />,
    },
  ]

  return (
    <div className="profile__user w-[80%]">
      <h2 className='text-2xl text-gray-700'>Thông tin tài khoản</h2>
      <div className='user__info bg-white p-5 rounded shadow-lg mt-10 flex flex-col gap-y-10'>
        <h3 className='text-lg text-gray-400'>Thông tin cá nhân</h3>
        <div className='user__basic__info flex flex-row gap-x-5 items-center'>
          <div className='user__avatar'>
            {imageUrl === '' ? (
              <>
                <ImgCrop
                  rotationSlider={true}
                  quality={1}
                  modalTitle='Chỉnh sửa ảnh'
                  modalWidth={1000}
                >
                  <Upload
                    name="avatar"
                    listType="picture-circle"
                    className="avatar-uploader"
                    showUploadList={false}
                    beforeUpload={beforeUpload}
                    onChange={handleChange}
                  >
                    {uploadButton}
                  </Upload>
                </ImgCrop>
              </>
            ) : (
              <>
                <ConfigProvider
                  theme={{
                    components: {
                      Dropdown: {
                        paddingBlock: '10px',
                      },
                    },
                  }}
                >
                  <Dropdown
                    trigger={['click']}
                    menu={{ items: [...alreadyUploadedOptions] }}
                    placement="bottomRight"
                  >
                    <Space className="cursor-pointer relative w-[150px] h-[150px]">
                      <img
                        src={imageUrl}
                        alt="avatar"
                        className="rounded-full w-full"
                      />
                      <EditOutlined
                        style={{
                          position: 'absolute',
                          bottom: '12px',
                          right: '12px',
                          fontSize: '25px',
                          zIndex: '5',
                        }}
                      />
                    </Space>
                  </Dropdown>
                </ConfigProvider>
              </>
            )}
          </div>
          <div className='fullName__nickName flex flex-col w-[40%] gap-y-5'>
            <div className='fullName__input__wrapper flex flex-row gap-x-5'>
              <span className='fullName__label w-[30%] text-lg text-gray-400'>Họ & Tên</span>
              <Input value={fullName} onChange={e => setfullName(e.target.value)} className='fullName__input__wrapper'/>
            </div>
            <div className='phoneNumber__input__wrapper flex flex-row gap-x-5'>
              <span className='phoneNumber__label w-[30%] text-lg text-gray-400'>Số điện thoại</span>
              <Input value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} className='nickName__input__wrapper'/>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
