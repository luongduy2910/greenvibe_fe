import { ProfileCategorySideBar } from '@/components/ProfileCategorySideBar/ProfileCategorySideBar'
import React from 'react'
import { HeaderCustom } from '@/components/HeaderCustom/HeaderCustom';

export default function ProfileCustomerLayout({children} : Readonly<{children: React.ReactNode}>) {
  return (
    <div className='min-h-screen w-full bg-[#F5F5FB]'>
        <HeaderCustom/>
        <div className='profile__customer___page flex flex-row w-[80%] mx-auto p-10 gap-x-20'>
            <ProfileCategorySideBar/>
            {children}
        </div>
    </div>
  )
}
