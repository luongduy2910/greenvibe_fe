import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import { AntdRegistry } from '@ant-design/nextjs-registry'
import ReduxToolkitProvider from '@/providers/Redux-Toolkit-Provider'
import { headers } from 'next/headers'
import { CategoryService } from '@/services/CategoryService'
import { SubCategoryService } from '@/services/SubCategoryService'
import { SearchService } from '@/services/SearchService'
import { Category } from '@/types/category'
import { HeaderCustom } from '@/components/HeaderCustom/HeaderCustom'
import { BgOverLayout } from '@/components/BgOverLayout/BgOverLayout'
import { DispatchAction } from '@/components/DispatchAction/DispatchAction'
import { CartDrawer } from '@/components/CartDrawer/CartDrawer'
import { CartDetails } from '@/types/cart'
import { CartService } from '@/services/CartService'
import { LoadingLogo } from '@/components/LoadingLogo/LoadingLogo'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Address } from '@/types/address'
import { ShippingAddressService } from '@/services/ShippingAddressService'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
}

// thời gian tái tạo lại trang
export const revalidate = 10


//NOTE - Liệt kê các api cần phải gọi để xây dựng headers

async function getCategories(): Promise<Category[]> {
  const categories = await CategoryService.getAllCategories();
  return categories;
}

async function getSubCategories(): Promise<SubCategory[]> {
  const subCategories = await SubCategoryService.getAll();
  return subCategories;
}

// async function getSuggestions(): Promise<Suggestion[]> {
//   return await SearchService.getListSuggestions();
// }

// async function getCarts() : Promise<CartDetails[] | null> {
//   return await CartService.getCartByUsername();
// }

// async function getAddresses() : Promise<Address[]> {
//   return await ShippingAddressService.getAddress();
// }



// NOTE - Kết thúc -------------------------------

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>
) {

  const categories = await getCategories();
  const subCategories = await getSubCategories();
  //const suggestions = await getSuggestions();
  // const cartDetails = await getCarts();
  //const addresses = await getAddresses();

  console.log('categories', categories)
  console.log('sub categories', subCategories)
  // console.log('cart' , cartDetails);
  //console.log("addresses", addresses);
  

  return (
    <html lang="en">
      <AntdRegistry>
        <ReduxToolkitProvider>
          <body className={inter.className}>
            {/* Dựng 1 component -> sau khi fetch data ở root layout -> dispatch data lên redux  */}
            <DispatchAction categories={categories} subCategories={subCategories} />
            {children}
            <BgOverLayout/>
          </body>
        </ReduxToolkitProvider>
      </AntdRegistry>
    </html>
  )
}
